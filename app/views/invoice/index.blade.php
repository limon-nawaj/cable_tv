@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Invoice <small></small>
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('invoice/index') }}">
                Invoice
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

    <div class="row">
      <div class="col-md-6">
        
        {{ Form::open(array('route'=>'new_invoice', 'method'=>'GET')) }}
          <div class="form-group">
            <div class="col-md-2">{{ Form::label('area', 'Area') }}</div>
            <div class="col-md-8">{{ Form::select('area', $area, '', array('class'=>'form-control'), Input::old('area'))}}</div>
            <div class="col-md-2">{{ Form::submit('Search', array('class'=>'btn green')) }}</div>
          </div>
        {{ Form::close() }}
      </div>
    </div>

    
@endsection