@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Pay Channel Invoice <small></small>
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('channel_invoice/index') }}">
                Pay Channel Invoices
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->


    @if($pay_channels->count())
      {{ Form::open(array('route'=>'save_channel_invoice', 'method'=>'POST')) }}
        <!-- Month and year field start -->
        <div class="row">
          <div class="col-md-12">
          {{ Form::hidden('area_id', $area_id) }}
          {{ Form::hidden('type', 'monthly') }}
            <div class="col-md-5">
              <div class="form-group">
                <div class="col-md-2">
                  {{ Form::label('month', 'Month') }}
                </div>
                <div class="col-md-10">
                  {{ Form::select('month', $month, '', array('class'=>'form-control'), Input::old('month')) }}                  
                </div>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <div class="col-md-2">
                  {{ Form::label('year', 'Year') }}
                </div>
                <div class="col-md-9">
                  {{ Form::select('year', $year, '', array('class'=>'form-control'), Input::old('year')) }}
                </div>
              </div>
            </div>
            <div class="col-md-2">
              {{ Form::submit('Generate Invoice', array('class'=>'btn green')) }}
            </div>
          </div>
        </div>
        <!-- Month and year field end -->
      {{ Form::close() }}  

      <div class="row">
        <div class="col-md-12">
        	<h2>Sub channel List of {{ Area::find($area_id)->name }}</h2>
            <table class="table">
              <tr>
                <th>CID</th>
                <th>Name</th>
                <th>Number of TV</th>
                <th>Monthly Fee</th>
                <th>Vat</th>
              </tr>

              @foreach($pay_channels as $pay_channel)
                @if($pay_channel->status == 1)
                	<tr>
	                  <td>ID</td>
	                  <td>{{ $pay_channel->first_name }} {{ $pay_channel->last_name }}</td>
	                  <td>{{ $pay_channel->num_of_tv }}</td>
                    <td>{{ $pay_channel->monthly_fee }}</td>
	                  <td>{{ $pay_channel->monthly_vat }}%</td>
	                </tr>
                @endif
              @endforeach
            </table>
        </div>
      </div>
    @endif
@endsection