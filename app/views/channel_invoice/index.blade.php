@extends('commons.default')

@section('content')
  <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Channel Invoice <small></small>
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('channel_invoice/index') }}">
                Channel Invoices
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->


    @if($pay_channels->count())
      {{ Form::open(array('route'=>'save_channel_invoice', 'method'=>'POST')) }}
        <!-- Month and year field start -->
        <div class="row">
          <div class="col-md-12">
          {{ Form::hidden('type', 'monthly') }}
            <div class="col-md-3">
              <div class="form-group">
                <div class="col-md-2">
                  {{ Form::label('month', 'Month') }}
                </div>
                <div class="col-md-10">
                  {{ Form::select('month', $month, '', array('class'=>'form-control'), Input::old('month')) }}                  
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <div class="col-md-2">
                  {{ Form::label('year', 'Year') }}
                </div>
                <div class="col-md-9">
                  {{ Form::select('year', $year, '', array('class'=>'form-control'), Input::old('year')) }}
                </div>
              </div>
            </div>
            <div class="col-md-2">
            </div>
          </div>
        </div>
        <!-- Month and year field end -->
      

      <div class="row">
        <div class="col-md-12">
          <h2>Pay Channel List
          </h2>
            <table class="table">
              <tr>
                <th>#</th>
                <th>CID</th>
                <th>Name</th>
                <th>Monthly Fee</th>
                <th>Vat</th>
                <th>Debit/Credit</th>
              </tr>

              @foreach($pay_channels as $channel)
                @if($channel->status == 1)
                  
                    <tr>
                      <td>
                          <input type="checkbox" name="formChannel[]" value="{{$channel->id}}" />
                      </td>
                      <td>{{ $channel->channel_id }}</td>
                      <td>{{ $channel->first_name }} {{ $channel->last_name }}</td>
                      <td>{{ $channel->monthly_fee }}</td>
                      <td>{{ $channel->monthly_vat }}%</td>
                      <td>
                        {{ ChannelBill::where('channel_id', $channel->id)->orderBy('id', 'desc')->first()->total }}
                      </td>
                    </tr>
                @endif
              @endforeach
            </table>
            {{ Form::submit('Generate Invoice', array('class'=>'btn green', 'name'=>'formSubmit')) }}
        </div>
      </div>
      {{ Form::close() }} 
    @endif

@endsection