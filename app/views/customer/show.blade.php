@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Customer
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('customer/index') }}">
                Customer
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('customer/'.$customer->id.'/edit') }}">
                {{ $customer->first_name }}
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    <!-- END PAGE HEADER-->

    

    <div class="col-md-7">
      <h2>{{$customer->first_name}} {{$customer->last_name}}</h2>
      <table class="table">
        <tr>
          <th>Month</th>
          <th>Year</th>
          <th>Area</th>
          
          <th>Debit</th>
          <th>Credit</th>
          <th>Total</th>
        </tr>

        @foreach($invoices as $invoice)
          <tr>
            <td>{{ $invoice->month }}</td>
            <td>{{ $invoice->year }}</td>
            <td>{{ Area::find($invoice->area_id)->name }}</td>
            <td>{{ Bill::where('invoice_id', $invoice->id)->where('credit', 0)->sum('debit') }}</td>
            <td>{{ Bill::where('invoice_id', $invoice->id)->where('debit', 0)->sum('credit') }}</td>
            <td>{{ Bill::where('invoice_id', $invoice->id)->where('debit', 0)->sum('credit')-Bill::where('invoice_id', $invoice->id)->where('credit', 0)->sum('debit') }}</td>
          </tr>
        @endforeach
      </table>
    </div>

    <div class="col-md-5">
      <button class="btn btn-primary" data-toggle="modal" data-target="#invoice">Generate Invoice</button>
      <button class="btn btn-info" data-toggle="modal" data-target="#myModal">Paid</button>
      <h3>Customer Info</h3>
      <div class="col-md-4">Customer Id </div>
      <div class="col-md-8">: {{ $customer->cust_id }}</div>
      <div class="col-md-4">Full Name </div>
      <div class="col-md-8">: {{$customer->first_name}} {{$customer->last_name}}</div>

      <div class="col-md-4">Address </div>
      <div class="col-md-8">
        @if(empty($customer->block))
          : {{ $customer->flat }}, {{ $customer->house }}, {{ $customer->road }}, {{ Area::find($customer->area)->name }}
        @else
          : {{ $customer->flat }}, {{ $customer->house }}, {{ $customer->road }}, {{ $customer->block }}, {{ Area::find($customer->area)->name }}
        @endif
      </div>

      <div class="col-md-4">Phone </div>
      <div class="col-md-8">: {{ $customer->phone }}</div>

      <div class="col-md-4">Mobile </div>
      <div class="col-md-8">: {{ $customer->mobile }}</div>

      <div class="col-md-4">Join Date </div>
      <div class="col-md-8">: {{ $customer->connection_date }}</div>

      <div class="col-md-4">Connection Fee </div>
      <div class="col-md-8">: {{ $customer->connection_fee }}</div>

      <div class="col-md-4">Total TV </div>
      <div class="col-md-8">: {{ $customer->num_of_tv }}</div>

      <div class="col-md-4">Monthly Fee </div>
      <div class="col-md-8">: {{ $customer->monthly_fee }}</div>

      <div class="col-md-4">Status </div>
      <div class="col-md-8">
        @if($customer->status == 1)
          : Connected
        @else
          : Not Connected
        @endif
      </div>

      <div class="col-md-4">Note </div>
      <div class="col-md-8">: {{ $customer->note }}</div>

    </div>

    <?php $b = Bill::where('customer_id', $customer->id)->orderBy('id', 'desc')->first() ?>

    <!-- get paid -->
    <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel">Get Paid</h4>
            </div>
            {{ Form::open(array('route'=>'paid_bill', 'method'=>'PUT', 'id'=>$invoice->id)) }}
            <div class="modal-body">
              
                {{ Form::hidden('invoice_id', $b->invoice_id) }}
                {{ Form::hidden('customer_id', $customer->id) }}
                {{ Form::label('credit', 'Credit') }}<br>
                {{ Form::text('credit', '', array('class'=>'form-control'), Input::old('credit')) }}
                
                {{ Form::hidden('page', 'individual') }}
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <!-- <button type="button" onclick="this.form.submit();" class="btn btn-primary">Save</button> -->
              {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
      <!-- Modal -->
    <!-- get paid -->



    <!-- get Invoice -->
    <!-- Modal -->
    <div class="modal fade" id="invoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          {{ Form::model(array('method'=>'put', 'route'=>array('save_generate_invoice', $customer->id))) }}
            <div class="modal-body">
              
                {{ Form::hidden('type', 'monthly') }}
                {{ Form::hidden('customer_id', $customer->id)}}

                {{ Form::label('month', 'Month') }}
                {{ Form::select('month', $month, '', array('class'=>'form-control'), Input::old('month')) }}

                {{ Form::label('year', 'Year') }}
                {{ Form::select('year', $year, '', array('class'=>'form-control'), Input::old('year')) }}
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <!-- <button type="button" onclick="this.form.submit();" class="btn btn-primary">Save</button> -->
              {{ Form::submit('Generate Invoice', array('class'=>'btn btn-
              primary')) }}
            </div>
            {{ Form::close() }}
        </div>
      </div>
    </div>
    <!-- get Invoice -->

@endsection