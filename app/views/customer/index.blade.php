@extends('commons.default')

@section('content')

	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Customer
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('customer/index') }}">
                Customer
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

	<div class="col-sm-12">
		
	<div class="pull-right">
		<a href="{{ URL::to('customer/create') }}" class="btn btn-success">Add New customer</a>
	</div>

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
		  <li class="active"><a href="#current_customer" role="tab" data-toggle="tab">Current customer</a></li>
		  <li><a href="#x-customer" role="tab" data-toggle="tab">x-customer</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div class="tab-pane active" id="current_customer">
		  		<table class="table">
					<tr>
						<th>Customer Id</th>
						<th>Name</th>
						<th>Address</th>
						<th>Contact Number</th>
						<th>Monthly fee</th>
						<th>Monthly Vat</th>
						<th>Additional fee</th>
						<th>Discount</th>
						<th>Action</th>
					</tr>

					@foreach($customers as $customer)
						@if($customer->status == 1)
							<tr>
								<td>{{ $customer->cust_id }}</td>
								<td>
									{{ $customer->first_name }} {{ $customer->last_name }}	
								</td>
								<td>
									@if(empty($customer->block))
										{{ $customer->flat }}, {{ $customer->house }}, {{ $customer->road }}, {{ Area::find($customer->area)->name }}
									@else
										{{ $customer->flat }}, {{ $customer->house }}, {{ $customer->road }}, {{ $customer->block }}, {{ Area::find($customer->area)->name }}
									@endif
								</td>
								<td>
									@if(empty($customer->phone))
										{{ $customer->mobile }}
									@else
										{{ $customer->phone }}, {{ $customer->mobile }}
									@endif
								</td>
								<td>{{ $customer->monthly_fee }}</td>
								<td>{{ $customer->monthly_vat }}</td>
								<td>{{ $customer->additional_fee }}</td>
								<td>{{ $customer->discount }}</td>
								<td>
									{{ HTML::linkRoute('edit_customer', 'Edit', $customer->id) }} | 
									<a href="{{$customer->id}}">Details</a>
								</td>
							</tr>
						@endif
					@endforeach
				</table>
		  </div>
		  <div class="tab-pane" id="x-customer">
		  		<table class="table">
					<tr>
						<th>Customer Id</th>
						<th>Name</th>
						<th>Address</th>
						<th>Contact Number</th>
						<th>Monthly fee</th>
						<th>Monthly Vat</th>
						<th>Additional fee</th>
						<th>Discount</th>
						<th>Total Fee</th>
						<th>Action</th>
					</tr>

					@foreach($customers as $customer)
						@if($customer->status == 0)
							<tr>
								<td>{{ $customer->cust_id }}</td>
								<td>{{ $customer->first_name }} {{ $customer->last_name }}</td>
								<td>
									@if(empty($customer->block))
										{{ $customer->flat }}, {{ $customer->house }}, {{ $customer->road }}, {{ Area::find($customer->area)->name }}
									@else
										{{ $customer->flat }}, {{ $customer->house }}, {{ $customer->road }}, {{ $customer->block }}, {{ Area::find($customer->area)->name }}
									@endif
								</td>
								<td>
									@if(empty($customer->phone))
										{{ $customer->mobile }}
									@else
										{{ $customer->phone }}, {{ $customer->mobile }}
									@endif
								</td>
								<td>{{ $customer->monthly_fee }}</td>
								<td>{{ $customer->monthly_vat }}</td>
								<td>{{ $customer->additional_fee }}</td>
								<td>{{ $customer->discount }}</td>
								<td>000</td>
								<td>
									{{ HTML::linkRoute('edit_customer', 'Edit', $customer->id) }}
								</td>
							</tr>
						@endif
					@endforeach
				</table>
		  </div>
		</div>
	</div>
@endsection