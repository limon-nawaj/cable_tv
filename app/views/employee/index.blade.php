@extends('commons.default')

@section('content')
	<div class="col-sm-12">
		
	<a href="{{ URL::to('employee/create') }}" class="btn btn-success">Add New Employee</a><br>

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
		  <li class="active"><a href="#current_employee" role="tab" data-toggle="tab">Current Employee</a></li>
		  <li><a href="#x-employee" role="tab" data-toggle="tab">x-employee</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div class="tab-pane active" id="current_employee">
		  		<table class="table">
					<tr>
						<td>Name</td>
						<td>Email</td>
						<td>Join Date</td>
						<td>Designation</td>
						<td>Basic Salary</td>
						<td>Other Salary</td>
						<td>Total Salary</td>
						<td>Action</td>
					</tr>

					@foreach($employees as $employee)
						@if($employee->status == 1)
							<tr>
								<td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
								<td>{{ $employee->user->email }}</td>
								<td>{{ $employee->join_date }}</td>
								<td>{{ $employee->designation->title }}</td>
								<td>{{ $employee->basic_salary }}</td>
								<td>{{ $employee->other_salary }}</td>
								<td>{{ $employee->basic_salary+$employee->other_salary }}</td>
								<td>
									{{ HTML::linkRoute('edit_employee', 'Edit', $employee->id) }}
								</td>
							</tr>
						@endif
					@endforeach
				</table>
		  </div>
		  <div class="tab-pane" id="x-employee">
		  		<table class="table">
					<tr>
						<td>Name</td>
						<td>Email</td>
						<td>Join Date</td>
						<td>Designation</td>
						<td>Basic Salary</td>
						<td>Other Salary</td>
						<td>Total Salary</td>
						<td>Action</td>
					</tr>

					@foreach($employees as $employee)
						@if($employee->status == 0)
							<tr>
								<td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
								<td>{{ $employee->user->email }}</td>
								<td>{{ $employee->join_date }}</td>
								<td>{{ $employee->designation->title }}</td>
								<td>{{ $employee->basic_salary }}</td>
								<td>{{ $employee->other_salary }}</td>
								<td>{{ $employee->basic_salary+$employee->other_salary }}</td>
								<td>
									{{ HTML::linkRoute('edit_employee', 'Edit', $employee->id) }}
								</td>
							</tr>
						@endif
					@endforeach
				</table>
		  </div>
		</div>
	</div>
@endsection