@extends('commons.default')

@section('content')
	<h1>Add New Employee</h1>
	<div class="col-md-8">
		{{ Form::open(array('route'=>'save_employee', 'method'=>'POST')) }}
			<div class="form-group">
				{{ Form::label('first_name', 'First Name') }}<br>
				{{ Form::text('first_name', '', array('class'=>'form-control'), Input::old('first_name')) }}
			</div>

			<div class="form-group">
				{{ Form::label('last_name', 'Last Name') }}<br>
				{{ Form::text('last_name', '', array('class'=>'form-control'), Input::old('last_name')) }}
			</div>

			
			<div class="form-group">
				{{ Form::label('email', 'Email') }}<br>
				{{ Form::text('email', '', array('class'=>'form-control'), Input::old('email')) }}
			</div>

			<div class="form-group">
				{{ Form::label('password', 'Password') }}<br>
				{{ Form::password('password', array('class'=>'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::label('mobile', 'Mobile') }}<br>
				{{ Form::text('mobile', '', array('class'=>'form-control'), Input::old('mobile')) }}
			</div>

			<div class="form-group">
				{{ Form::label('basic_salary', 'basic_salary') }}<br>
				{{ Form::text('basic_salary', '', array('class'=>'form-control'), Input::old('basic_salary')) }}
			</div>

			<div class="form-group">
				{{ Form::label('other_salary', 'other_salary') }}<br>
				{{ Form::text('other_salary', '', array('class'=>'form-control'), Input::old('other_salary')) }}
			</div>

			<div class="form-group">
                {{ Form::label('join_date', 'Start time') }} <br />
                <div class='input-group date' id="datetimepicker5" data-date-format="yyyy-mm-dd">
                    {{ Form::text('join_date', '', array('class'=>'form-control'), Input::old('join_date')) }}
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('designation_id', 'Designation') }} <br />
                {{ Form::select('designation_id', $designation, '', array('class'=>'form-control'), Input::old('designation_id'))}}
            </div>

            <div class="form-group">
				{{ Form::label('present_address', 'present_address') }}<br>
				{{ Form::text('present_address', '', array('class'=>'form-control'), Input::old('present_address')) }}
			</div>

			<div class="form-group">
				{{ Form::label('permanent_address', 'permanent_address') }}<br>
				{{ Form::text('permanent_address', '', array('class'=>'form-control'), Input::old('permanent_address')) }}
			</div>

			<div class="form-group">
				{{ Form::label('note', 'note') }}<br>
				{{ Form::text('note', '', array('class'=>'form-control'), Input::old('note')) }}
			</div>

			<p>{{ Form::submit('Add user', array('class'=>'btn btn-danger')) }}</p>
		{{ Form::close() }}
	</div>
@endsection