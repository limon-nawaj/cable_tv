@extends('commons.default')

@section('content')
	<h1>Add New Employee</h1>
	<div class="col-md-8">
		{{ Form::model($employee, array('method' => 'put', 'route' => array('update_employee', $employee->id))) }}
			<div class="form-group">
				{{ Form::label('first_name', 'First Name') }}<br>
				{{ Form::text('first_name', $employee->first_name, array('class'=>'form-control'), Input::old('first_name')) }}
			</div>

			<div class="form-group">
				{{ Form::label('last_name', 'Last Name') }}<br>
				{{ Form::text('last_name', $employee->last_name, array('class'=>'form-control'), Input::old('last_name')) }}
			</div>

			
			<div class="form-group">
				{{ Form::label('email', 'Email') }}<br>
				{{ Form::text('email', $employee->user->email, array('class'=>'form-control', 'disabled'=>'disabled'), Input::old('email')) }}
			</div>

			<div class="form-group">
				{{ Form::label('password', 'Password') }}<br>
				{{ Form::password('password', array('class'=>'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::label('mobile', 'Mobile') }}<br>
				{{ Form::text('mobile', $employee->mobile, array('class'=>'form-control'), Input::old('mobile')) }}
			</div>

			<div class="form-group">
				{{ Form::label('basic_salary', 'basic_salary') }}<br>
				{{ Form::text('basic_salary', $employee->basic_salary, array('class'=>'form-control'), Input::old('basic_salary')) }}
			</div>

			<div class="form-group">
				{{ Form::label('other_salary', 'other_salary') }}<br>
				{{ Form::text('other_salary', $employee->other_salary, array('class'=>'form-control'), Input::old('other_salary')) }}
			</div>

			<div class="form-group">
                {{ Form::label('join_date', 'Start time') }} <br />
                <div class='input-group date' id="datetimepicker5" data-date-format="yyyy-mm-dd">
                    {{ Form::text('join_date', $employee->join_date, array('class'=>'form-control'), Input::old('join_date')) }}
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('designation_id', 'Designation') }} <br />
                {{ Form::select('designation_id', $designation, Input::old('designation_id'), array('class'=>'form-control'))}}
            </div>

            <div class="form-group">
				{{ Form::label('present_address', 'Present Address') }}<br>
				{{ Form::text('present_address', $employee->present_address, array('class'=>'form-control'), Input::old('present_address')) }}
			</div>

			<div class="form-group">
				{{ Form::label('permanent_address', 'Permanent Address') }}<br>
				{{ Form::text('permanent_address', $employee->permanent_address, array('class'=>'form-control'), Input::old('permanent_address')) }}
			</div>

			<div class="form-group">
				{{ Form::label('note', 'Note') }}<br>
				{{ Form::text('note', $employee->note, array('class'=>'form-control'), Input::old('note')) }}
			</div>

			<div class="form-group">
				{{ Form::label('status', 'Active') }}
				@if($customer->status==1)
					{{ Form::hidden('status', false) }}
					{{ Form::checkbox('status', true) }}Current Employee
				@else
					{{ Form::hidden('status', false) }}
					{{ Form::checkbox('status', true) }}X-Employee
				@endif
			</div>

			<p>{{ Form::submit('Update Employee', array('class'=>'btn btn-danger')) }}</p>
		{{ Form::close() }}
	</div>
@endsection