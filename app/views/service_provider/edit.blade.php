@extends('commons.default')

@section('content')
	<h1>Edit {{ $provider->first_name }}</h1>
	{{ HTML::ul($errors->all()) }}
	<div class="col-md-8">
		{{ Form::model($provider, array('method' => 'put', 'route' => array('update_service_provider', $provider->id))) }}
			<div class="form-group">
				{{ Form::label('first_name', 'First Name') }}<br>
				{{ Form::text('first_name', $provider->first_name, array('class'=>'form-control'), Input::old('first_name')) }}
			</div>

			<div class="form-group">
				{{ Form::label('last_name', 'Last Name') }}<br>
				{{ Form::text('last_name', $provider->last_name, array('class'=>'form-control'), Input::old('last_name')) }}
			</div>

			<div class="form-group">
				{{ Form::label('company', 'Company Name') }}<br>
				{{ Form::text('company', $provider->company, array('class'=>'form-control'), Input::old('company')) }}
			</div>

			{{ Form::hidden('role', 'service_provider') }}

			<div class="form-group">
				{{ Form::label('email', 'Email') }}<br>
				{{ Form::text('email', $provider->user->email, array('class'=>'form-control', 'disabled'=>'disabled'), Input::old('email')) }}
			</div>

			<div class="form-group">
				{{ Form::label('password', 'Password') }}<br>
				{{ Form::password('password', array('class'=>'form-control')) }}
			</div>

			{{ Form::hidden('status', '1') }}

			<div class="form-group">
				{{ Form::label('mobile', 'Mobile') }}<br>
				{{ Form::text('mobile', $provider->mobile, array('class'=>'form-control'), Input::old('mobile')) }}
			</div>


			<div class="form-group">
                {{ Form::label('join_date', 'Start time') }} <br />
                <div class='input-group date' id="datetimepicker5" data-date-format="yyyy-mm-dd">
                    {{ Form::text('join_date', $provider->join_date, array('class'=>'form-control'), Input::old('join_date')) }}
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </div>
            </div>

            <div class="form-group">
				{{ Form::label('status', 'Active') }}
				@if($provider->status==1)
					{{ Form::hidden('status', false) }}
					{{ Form::checkbox('status', true) }}Connected
				@else
					{{ Form::hidden('status', false) }}
					{{ Form::checkbox('status', true) }}Not Connected
				@endif
			</div>

			<div class="form-group">
				{{ Form::label('service', 'Service') }}
				@if($provider->employee==1)
					{{ Form::hidden('employee', false) }}
					{{ Form::checkbox('employee', true) }}Employee
				@else
					{{ Form::hidden('employee', false) }}
					{{ Form::checkbox('employee', true) }}Employee
				@endif

				@if($provider->operator==1)
					{{ Form::hidden('operator', false) }}
					{{ Form::checkbox('operator', true) }}Operator
				@else
					{{ Form::hidden('operator', false) }}
					{{ Form::checkbox('operator', true) }}Operator
				@endif

				@if($provider->channel==1)
					{{ Form::hidden('channel', false) }}
					{{ Form::checkbox('channel', true) }}Channel
				@else
					{{ Form::hidden('channel', false) }}
					{{ Form::checkbox('channel', true) }}Channel
				@endif
			</div>


			<p>{{ Form::submit('Update', array('class'=>'btn btn-danger')) }}</p>
		{{ Form::close() }}
	</div>
@endsection