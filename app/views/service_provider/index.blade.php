@extends('commons.default')

@section('content')
	<h1>Service Providers</h1>

	<table class="table">
		<tr>
			<td>Name</td>
			<td>Email</td>
			<td>Mobile</td>
			<td>Company</td>
			<td>Join Date</td>
			<td>Status</td>
			<td>Action</td>
		</tr>

		@foreach ($service_providers as $provider)
			<tr>
				<td>{{ $provider->first_name }} {{ $provider->last_name }}</td>
				<td>{{ $provider->user->email }}</td>
				<td>{{ $provider->mobile }}</td>
				<td>{{ $provider->company }}</td>
				<td>{{ $provider->join_date }}</td>
				<td>
					@if($provider->status == 1)
						Active
					@else
						Inactive
					@endif
				</td>
				<td>
					{{ HTML::linkRoute('edit_service_provider', 'Edit', $provider->id) }}
					
				</td>
			</tr>	
		@endforeach
	</table>
@endsection