@extends('commons.default')

@section('content')
	<h1>Add Service Provider</h1>
	{{ HTML::ul($errors->all()) }}
	<div class="col-md-8">
		{{ Form::open(array('route'=>'save_service_provider', 'method'=>'POST')) }}
			<div class="form-group">
				{{ Form::label('first_name', 'First Name') }}<br>
				{{ Form::text('first_name', '', array('class'=>'form-control'), Input::old('first_name')) }}
			</div>

			<div class="form-group">
				{{ Form::label('last_name', 'Last Name') }}<br>
				{{ Form::text('last_name', '', array('class'=>'form-control'), Input::old('last_name')) }}
			</div>

			<div class="form-group">
				{{ Form::label('company', 'Company Name') }}<br>
				{{ Form::text('company', '', array('class'=>'form-control'), Input::old('company')) }}
			</div>

			{{ Form::hidden('role', 'service_provider') }}

			<div class="form-group">
				{{ Form::label('email', 'Email') }}<br>
				{{ Form::text('email', '', array('class'=>'form-control'), Input::old('email')) }}
			</div>

			<div class="form-group">
				{{ Form::label('password', 'Password') }}<br>
				{{ Form::password('password', array('class'=>'form-control')) }}
			</div>

			{{ Form::hidden('status', '1') }}

			<div class="form-group">
				{{ Form::label('mobile', 'Mobile') }}<br>
				{{ Form::text('mobile', '', array('class'=>'form-control'), Input::old('mobile')) }}
			</div>

			<div class="form-group">
                {{ Form::label('join_date', 'Start time') }} <br />
                <div class='input-group date' id="datetimepicker5" data-date-format="yyyy-mm-dd">
                    {{ Form::text('join_date', '', array('class'=>'form-control'), Input::old('join_date')) }}
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </div>
            </div>

            <div class="form-group">
				{{ Form::label('service', 'Service') }}<br>
				{{ Form::hidden('employee', false) }}
				{{ Form::checkbox('employee', true) }}Employee
				{{ Form::hidden('operator', false) }}
				{{ Form::checkbox('operator', true) }}Operator
				{{ Form::hidden('channel', false) }}
				{{ Form::checkbox('channel', true) }}Pay Channel
			</div>

			<p>{{ Form::submit('Add user', array('class'=>'btn btn-danger')) }}</p>
		{{ Form::close() }}
	</div>
@endsection