@extends('commons.default')

@section('content')
	{{ Form::model($designation, array('method'=>'put', 'route'=>array('update_designation', $designation->id))) }}
		<div class="form-group">
			{{ Form::label('title', 'Name of designation') }}
			{{ Form::text('title', $designation->title, array('class'=>'form-controller'), Input::old('title')) }}
		</div>

		<div class="form-group">
			{{ Form::label('status', 'Active') }}
			@if($designation->status==1)
				{{ Form::checkbox('status', true) }}
				{{ Form::hidden('status', false) }}
			@else
				{{ Form::checkbox('status', false) }}
				{{ Form::hidden('status', true) }}
			@endif
		</div>
		<p>{{ Form::submit('Add user', array('class'=>'btn btn-danger')) }}</p>
	{{ Form::close() }}
@endsection