@extends('commons.default')

@section('content')
	<div class="col-md-12">
		<div class="col-md-6">
			{{ Form::open(array('route'=>'save_designation', 'method'=>'POST')) }}
				<div class="form-group">
					{{ Form::label('title', 'Name of designation') }}
					{{ Form::text('title', '', array('class'=>'form-controller'), Input::old('title')) }}

				</div>
				<p>{{ Form::submit('Add user', array('class'=>'btn btn-danger')) }}</p>
			{{ Form::close() }}
		</div>

		<div class="col-md-6">
			<table class="table">
				<tr>
					<td>Name</td>
					<td>Status</td>
					<td>Action</td>
				</tr>

				@foreach ($designations as $designation)
					<tr>
						<td>{{$designation->title}}</td>
						<td>
							@if($designation->status==1)
								Active
							@else
								<font color="red">Inactive</font>
							@endif
						</td>
						<td>
							{{ HTML::linkRoute('edit_designation', 'Edit', $designation->id) }}
						</td>
					</tr>	
				@endforeach
			</table>
		</div>
	</div>
@endsection