@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Dashboard
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('employee/dashboard') }}">
                Home
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="dashboard-stat blue">
            <div class="visual">
              <i class="fa fa-comments"></i>
            </div>
            <div class="details">
              
              <div class="desc">
                 Customer Bill
              </div>
            </div>
            <a class="more" href="{{ URL::to('bill/index') }}">
               View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
        @if($provider->operator==1)
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat green">
              <div class="visual">
                <i class="fa fa-shopping-cart"></i>
              </div>
              <div class="details">
                <div class="desc">
                   Sub-Operator Bill
                </div>
              </div>
              <a class="more" href="{{ URL::to('operator_bill/index') }}">
                 View more <i class="m-icon-swapright m-icon-white"></i>
              </a>
            </div>
          </div>
        @endif
      </div>

      

@endsection
