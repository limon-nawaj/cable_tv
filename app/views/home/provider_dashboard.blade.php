@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Dashboard
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('home/provider_dashboard') }}">
                Home
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat blue">
            <div class="visual">
              <i class="fa fa-comments"></i>
            </div>
            <div class="details">
              <div class="number">
                 {{ Customer::where('provider_id', $provider->id)->count() }}
              </div>
              <div class="desc">
                 Customer
              </div>
            </div>
            <a class="more" href="{{ URL::to('customer/index') }}">
               View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
        @if($provider->operator==1)
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat green">
              <div class="visual">
                <i class="fa fa-shopping-cart"></i>
              </div>
              <div class="details">
                <div class="number">
                   {{ SubOperator::where('provider_id', $provider->id)->count() }}
                </div>
                <div class="desc">
                   Sub-Operator
                </div>
              </div>
              <a class="more" href="{{ URL::to('sub_operator/index') }}">
                 View more <i class="m-icon-swapright m-icon-white"></i>
              </a>
            </div>
          </div>
        @endif
        @if($provider->channel==1)
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat purple">
              <div class="visual">
                <i class="fa fa-globe"></i>
              </div>
              <div class="details">
                <div class="number">
                   {{ PayChannel::where('provider_id', $provider->id)->count() }}
                </div>
                <div class="desc">
                   Pay-Channel
                </div>
              </div>
              <a class="more" href="{{ URL::to('pay_channel/index') }}">
                 View more <i class="m-icon-swapright m-icon-white"></i>
              </a>
            </div>
          </div>
        @endif
        @if($provider->employee==1)
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat yellow">
              <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
              </div>
              <div class="details">
                <div class="number">
                   {{ Employee::where('provider_id', $provider->id)->count() }}
                </div>
                <div class="desc">
                   Employee
                </div>
              </div>
              <a class="more" href="{{ URL::to('employee/index') }}">
                 View more <i class="m-icon-swapright m-icon-white"></i>
              </a>
            </div>
          </div>
        @endif
      </div>

      <div class="col-md-12">
        <div class="col-md-6 col-sm-6">
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              New Customer
            </div>
          </div>
          <div class="portlet-body">
            <table class="table">
              <tr>
                <th>Customer Id</th>
                <th>Name</th>
                <th>Area</th>
              </tr>
              @foreach($customers as $customer)
                <tr>
                  <td>{{ $customer->cust_id }}</td>
                  <td>{{ $customer->first_name }} {{ $customer->last_name }}</td>
                  <td>{{ Area::find($customer->area)->name }}</td>
                </tr>
              @endforeach
            </table>  
          </div>
          
        </div>
      </div>

      @if($provider->operator==1)
        <div class="col-md-6 col-sm-6">
          <div class="portlet box blue">
            <div class="portlet-title">
              <div class="caption">
                New Sub-operator
              </div>
            </div>
            <div class="portlet-body">
              <table class="table">
                <tr>
                  <th>Sub-operator Id</th>
                  <th>Name</th>
                  <th>Area</th>
                </tr>
                @foreach($sub_operators as $sub_operator)
                  <tr>
                    <td>{{ $sub_operator->opt_id }}</td>
                    <td>{{ $sub_operator->first_name }} {{ $sub_operator->last_name }}</td>
                    <td>{{ Area::find($sub_operator->area)->name }}</td>
                  </tr>
                @endforeach
              </table>  
            </div>
            
          </div>
        </div>
      @endif


      @if($provider->channel==1)
        <div class="col-md-6 col-sm-6">
          <div class="portlet box blue">
            <div class="portlet-title">
              <div class="caption">
                New Pay Channel
              </div>
            </div>
            <div class="portlet-body">
              <table class="table">
                <tr>
                  <th>Pay Channel Id</th>
                  <th>Name</th>
                  <th>Area</th>
                </tr>
                @foreach($pay_channels as $pay_channel)
                  <tr>
                    <td>{{ $pay_channel->chan_id }}</td>
                    <td>{{ $pay_channel->first_name }} {{ $pay_channel->last_name }}</td>
                    <td>{{ Area::find($pay_channel->area)->name }}</td>
                  </tr>
                @endforeach
              </table>  
            </div>
            
          </div>
        </div>
      @endif

      @if($provider->employee==1)
        <div class="col-md-6 col-sm-6">
          <div class="portlet box blue">
            <div class="portlet-title">
              <div class="caption">
                New Employee
              </div>
            </div>
            <div class="portlet-body">
              <table class="table">
                <tr>
                  <th>Employee Id</th>
                  <th>Name</th>
                </tr>
                @foreach($employees as $employee)
                  <tr>
                    <td>{{ $employee->id }}</td>
                    <td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
                  </tr>
                @endforeach
              </table>  
            </div>
            
          </div>
        </div>
      @endif

@endsection
