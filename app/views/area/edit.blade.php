@extends('commons.default')

@section('content')
	
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Area
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('area/index') }}">
                Area
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

    <div class="row">
    	<div class="col-md-12">
    		{{ Form::model($area, array('method'=>'put', 'route'=>array('update_area', $area->id))) }}
				<div class="form-group">
					{{ Form::label('name', 'Name of area') }}
					{{ Form::text('name', $area->name, array('class'=>'form-controller'), Input::old('name')) }}
				</div>

				<div class="form-group">
					{{ Form::label('status', 'Active') }}
					@if($area->status==1)
						{{ Form::checkbox('status', true) }}
						{{ Form::hidden('status', false) }}
					@else
						{{ Form::checkbox('status', false) }}
						{{ Form::hidden('status', true) }}
					@endif
				</div>
				<p>{{ Form::submit('Add Area', array('class'=>'btn success')) }}</p>
			{{ Form::close() }}
    	</div>
    </div>
@endsection