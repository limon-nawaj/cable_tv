@extends('commons.default')

@section('content')
	
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Area
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('area/index') }}">
                Area
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				{{ Form::open(array('route'=>'save_area', 'method'=>'POST')) }}
					<div class="form-group">
						{{ Form::label('name', 'Name of area') }}
						{{ Form::text('name', '', array('class'=>'form-control'), Input::old('name')) }}

					</div>
					<p>{{ Form::submit('Add Area', array('class'=>'btn green')) }}</p>
				{{ Form::close() }}
			</div>

			<div class="col-md-6">
				<table class="table">
					<tr>
						<td>Name</td>
						<td>Status</td>
						<td>Action</td>
					</tr>

					@foreach ($areas as $area)
						<tr>
							<td>{{$area->name}}</td>
							<td>
								@if($area->status==1)
									Active
								@else
									<font color="red">Inactive</font>
								@endif
							</td>
							<td>
								{{ HTML::linkRoute('edit_area', 'Edit', $area->id) }}
							</td>
						</tr>	
					@endforeach
				</table>
			</div>
		</div>
	</div>
@endsection