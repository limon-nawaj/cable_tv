<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 2.0.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

{{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') }}
{{ HTML::style('plugins/font-awesome/css/font-awesome.min.css') }}
{{ HTML::style('plugins/bootstrap/css/bootstrap.min.css') }}
{{ HTML::style('plugins/uniform/css/uniform.default.css') }}


<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
{{ HTML::style('plugins/select2/select2.css') }}
{{ HTML::style('plugins/select2/select2-metronic.css') }}
<!-- END PAGE LEVEL PLUGIN STYLES -->

<!-- BEGIN THEME STYLES --> 
{{ HTML::style('css/style-metronic.css') }}
{{ HTML::style('css/style.css') }}
{{ HTML::style('css/style-responsive.css') }}
{{ HTML::style('css/plugins.css') }}
{{ HTML::style('css/themes/default.css') }}
{{ HTML::style('css/pages/login.css') }}
{{ HTML::style('css/custom.css') }}
<!-- END THEME STYLES -->

</head>
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img src="assets/img/logo-big.png" alt=""/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    @if(Session::has('message'))
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <p class="alert">{{ Session::get('message') }}</p>
      </div>
    @endif
    <!-- BEGIN LOGIN FORM -->
    {{ Form::open(array('route' =>'user_login', 'method' =>'POST')) }}
        <h3 class="form-title">Login to your account</h3>

        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>{{ Form::token() }}</span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <div class="input-icon">
                <i class="fa fa-user"></i>
                {{ Form::text('email', '', array('class'=>'form-control placeholder-no-fix', 'autocomplete'=>'off', 'placeholder'=>'Email'), Input::old('email')) }}
            </div>
            
            
        </div>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                {{ Form::password('password', array('class'=>'form-control placeholder-no-fix', 'autocomplete'=>'off', 'placeholder'=>'password'), Input::old('email')) }}
            </div>
        </div>
        <div class="form-actions">
        {{ Form::submit('Login', array('class'=>'btn green pull-right')) }}
        </div>
        
    {{ Form::close() }}
    <!-- END LOGIN FORM -->
    
    @if($admin->count()==0)
        <a href="admin/create">
            Register
        </a>
    @endif
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
     2014 &copy; Metronic. Admin Dashboard Template.
</div>

<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
    <script src="assets/plugins/excanvas.min.js"></script> 
    <![endif]-->
<script src="assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/scripts/core/app.js" type="text/javascript"></script>
<script src="assets/scripts/custom/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
        jQuery(document).ready(function() {     
          App.init();
          Login.init();
        });
    </script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>