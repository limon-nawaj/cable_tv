@extends('commons.default')

@section('content')

	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Sub-operator
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('sub_operator/index') }}">
                Sub-operator
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

	<div class="col-sm-12">
		
	<div class="pull-right">
		<a href="{{ URL::to('sub_operator/create') }}" class="btn btn-success">Add New sub_operator</a>
	</div>

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
		  <li class="active"><a href="#current_sub_operator" role="tab" data-toggle="tab">Current sub_operator</a></li>
		  <li><a href="#x-sub_operator" role="tab" data-toggle="tab">x-sub_operator</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div class="tab-pane active" id="current_sub_operator">
		  		<table class="table">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Address</th>
						<th>Contact Number</th>
						<th>Monthly fee</th>
						<th>Monthly Vat</th>
						<th>Action</th>
					</tr>

					@foreach($sub_operators as $sub_operator)
						@if($sub_operator->status == 1)
							<tr>
								<td>{{ $sub_operator->opt_id }}</td>
								<td>{{ $sub_operator->first_name }} {{ $sub_operator->last_name }}</td>
								<td>
									@if(empty($sub_operator->block))
										{{ $sub_operator->flat }}, {{ $sub_operator->house }}, {{ $sub_operator->road }}, {{ Area::find($sub_operator->area)->name }}
									@else
										{{ $sub_operator->flat }}, {{ $sub_operator->house }}, {{ $sub_operator->road }}, {{ $sub_operator->block }}, {{ Area::find($sub_operator->area)->name }}
									@endif
								</td>
								<td>
									@if(empty($sub_operator->phone))
										{{ $sub_operator->mobile }}
									@else
										{{ $sub_operator->phone }}, {{ $sub_operator->mobile }}
									@endif
								</td>
								<td>{{ $sub_operator->monthly_fee }}</td>
								<td>{{ $sub_operator->monthly_vat }}</td>
								<td>
									{{ HTML::linkRoute('edit_sub_operator', 'Edit', $sub_operator->id) }} | 
									<a href="{{$sub_operator->id}}">Details</a>
								</td>
							</tr>
						@endif
					@endforeach
				</table>
		  </div>
		  <div class="tab-pane" id="x-sub_operator">
		  		<table class="table">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Address</th>
						<th>Contact Number</th>
						<th>Monthly fee</th>
						<th>Monthly Vat</th>
						<th>Action</th>
					</tr>

					@foreach($sub_operators as $sub_operator)
						@if($sub_operator->status == 0)
							<tr>
								<td>{{ $sub_operator->opt_id }}</td>
								<td>{{ $sub_operator->first_name }} {{ $sub_operator->last_name }}</td>
								<td>
									@if(empty($sub_operator->block))
										{{ $sub_operator->flat }}, {{ $sub_operator->house }}, {{ $sub_operator->road }}, {{ Area::find($sub_operator->area)->name }}
									@else
										{{ $sub_operator->flat }}, {{ $sub_operator->house }}, {{ $sub_operator->road }}, {{ $sub_operator->block }}, {{ Area::find($sub_operator->area)->name }}
									@endif
								</td>
								<td>
									@if(empty($sub_operator->phone))
										{{ $sub_operator->mobile }}
									@else
										{{ $sub_operator->phone }}, {{ $sub_operator->mobile }}
									@endif
								</td>
								<td>{{ $sub_operator->monthly_fee }}</td>
								<td>{{ $sub_operator->monthly_vat }}</td>
								<td>
									{{ HTML::linkRoute('edit_sub_operator', 'Edit', $sub_operator->id) }} | 
									<a href="{{$sub_operator->id}}">Details</a>
								</td>
							</tr>
						@endif
					@endforeach
				</table>
		  </div>
		</div>
	</div>
@endsection