@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Sub-Operator
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('sub_operator/index') }}">
                Sub-Operator
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('sub_operator/'.$sub_operator->id.'/edit') }}">
                {{ $sub_operator->first_name }}
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    <!-- END PAGE HEADER-->

    

    <div class="col-md-7">
      <h2>{{$sub_operator->first_name}} {{$sub_operator->last_name}}</h2>
      <table class="table">
        <tr>
          <th>Month</th>
          <th>Year</th>
          <th>Area</th>
          <th>Debit</th>
          <th>Credit</th>
          <th>Total</th>
        </tr>

        @foreach($operator_invoices as $operator_invoice)
          <tr>
            <td>{{ $operator_invoice->month }}</td>
            <td>{{ $operator_invoice->year }}</td>
            <td>{{ Area::find($operator_invoice->area_id)->name }}</td>
            <td>{{ OperatorBill::where('invoice_id', $operator_invoice->id)->where('credit', 0)->sum('debit') }}</td>
            <td>{{ OperatorBill::where('invoice_id', $operator_invoice->id)->where('debit', 0)->sum('credit') }}</td>
            <td>{{ OperatorBill::where('invoice_id', $operator_invoice->id)->where('debit', 0)->sum('credit')-Bill::where('invoice_id', $operator_invoice->id)->where('credit', 0)->sum('debit') }}</td>
          </tr>
        @endforeach
      </table>
    </div>

    <div class="col-md-5">
      <button class="btn btn-primary" data-toggle="modal" data-target="#invoice">Generate Invoice</button>
      <button class="btn btn-info" data-toggle="modal" data-target="#myModal">Paid</button>
      <h3>Sub-Operator Info</h3>
      <div class="col-md-4">Sub-Operator Id </div>
      <div class="col-md-8">: {{ $sub_operator->opt_id }}</div>
      <div class="col-md-4">Full Name </div>
      <div class="col-md-8">: {{$sub_operator->first_name}} {{$sub_operator->last_name}}</div>

      <div class="col-md-4">Address </div>
      <div class="col-md-8">
        @if(empty($sub_operator->block))
          : {{ $sub_operator->flat }}, {{ $sub_operator->house }}, {{ $sub_operator->road }}, {{ Area::find($sub_operator->area)->name }}
        @else
          : {{ $sub_operator->flat }}, {{ $sub_operator->house }}, {{ $sub_operator->road }}, {{ $sub_operator->block }}, {{ Area::find($sub_operator->area)->name }}
        @endif
      </div>

      <div class="col-md-4">Phone </div>
      <div class="col-md-8">: {{ $sub_operator->phone }}</div>

      <div class="col-md-4">Mobile </div>
      <div class="col-md-8">: {{ $sub_operator->mobile }}</div>

      <div class="col-md-4">Join Date </div>
      <div class="col-md-8">: {{ $sub_operator->connection_date }}</div>

      <div class="col-md-4">Connection Fee </div>
      <div class="col-md-8">: {{ $sub_operator->connection_fee }}</div>

      <div class="col-md-4">Total TV </div>
      <div class="col-md-8">: {{ $sub_operator->num_of_tv }}</div>

      <div class="col-md-4">Monthly Fee </div>
      <div class="col-md-8">: {{ $sub_operator->monthly_fee }}</div>

      <div class="col-md-4">Status </div>
      <div class="col-md-8">
        @if($sub_operator->status == 1)
          : Connected
        @else
          : Not Connected
        @endif
      </div>

      <div class="col-md-4">Note </div>
      <div class="col-md-8">: {{ $sub_operator->note }}</div>

    </div>

    <?php $b = OperatorBill::where('operator_id', $sub_operator->id)->orderBy('id', 'desc')->first() ?>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Get Paid</h4>
          </div>
          {{ Form::open(array('route'=>'paid_operator_bill', 'method'=>'PUT', 'id'=>$operator_invoice->id)) }}
          <div class="modal-body">
            
              {{ Form::hidden('invoice_id', $b->invoice_id) }}
              {{ Form::hidden('operator_id', $sub_operator->id) }}
              {{ Form::label('credit', 'Credit') }}<br>
              {{ Form::text('credit', '', array('class'=>'form-control'), Input::old('credit')) }}
              

              {{ Form::hidden('page', 'individual') }}
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" onclick="this.form.submit();" class="btn btn-primary">Save</button> -->
            {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
    <!-- Modal -->


    <!-- get Invoice -->
    <!-- Modal -->
    <div class="modal fade" id="invoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          {{ Form::model(array('method'=>'put', 'route'=>array('operator_invoice', $sub_operator->id))) }}
            <div class="modal-body">
              
                {{ Form::hidden('type', 'monthly') }}
                {{ Form::hidden('operator_id', $sub_operator->id)}}

                {{ Form::label('month', 'Month') }}
                {{ Form::select('month', $month, '', array('class'=>'form-control'), Input::old('month')) }}

                {{ Form::label('year', 'Year') }}
                {{ Form::select('year', $year, '', array('class'=>'form-control'), Input::old('year')) }}
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <!-- <button type="button" onclick="this.form.submit();" class="btn btn-primary">Save</button> -->
              {{ Form::submit('Generate Invoice', array('class'=>'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
        </div>
      </div>
    </div>
    <!-- get Invoice -->

@endsection