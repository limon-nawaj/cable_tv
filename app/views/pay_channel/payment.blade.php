@extends('commons.default')

@section('content')
	<div class="col-md-7">
		{{ Form::open(array('route'=>'save_channel_bill', 'method'=>'POST')) }}
	      <div class="modal-body">
	        
	          {{ Form::hidden('channel_id', $pay_channel->id) }}
	          {{ Form::label('credit', 'Credit') }}<br>
	          {{ Form::text('credit', '', array('class'=>'form-control', 'placeholder'=>'Credit'), Input::old('credit')) }}
	          {{ Form::label('type', 'Type') }}<br>
	          {{ Form::select('type', $type, '', array('class'=>'form-control'), Input::old('type')) }}
	          	<div class="form-group">
	                {{ Form::label('payment_date', 'Payment Date') }} <br />
	                <div class='input-group date' id="datetimepicker5" data-date-format="yyyy-mm-dd">
	                    {{ Form::text('payment_date', '', array('class'=>'form-control', 'placeholder'=>'Pyament Date'), Input::old('payment_date')) }}
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
	                </div>
	            </div>
	      </div>
	      <div class="modal-footer">
	        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
	        <!-- <button type="button" onclick="this.form.submit();" class="btn btn-primary">Save</button> -->
	        <a href="{{ URL::to('pay_channel/index') }}" class="btn btn-default">Close</a>
	        {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
	      </div>
	    {{ Form::close() }}
	</div>
@endsection