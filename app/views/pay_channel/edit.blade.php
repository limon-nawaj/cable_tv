@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Pay Channel
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('pay_channel/index') }}">
                Pay Channels
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('pay_channel/'.$pay_channel->id.'/edit') }}">
                {{ $pay_channel->first_name }} {{ $pay_channel->last_name }}
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
	
	<div class="col-md-8">
		{{ Form::model($pay_channel, array('method' => 'put', 'route' => array('update_pay_channel', $pay_channel->id))) }}
		
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('chan_id', 'Pay Channel Id') }}<br>
					{{ Form::text('chan_id', $pay_channel->chan_id, array('class'=>'form-control'), Input::old('chan_id')) }}
				</div>	
			</div>

			<div class="col-md-6">
				<div class="form-group">
	                {{ Form::label('connection_date', 'Start time') }} <br />
	                <div class='input-group date' id="datetimepicker5" data-date-format="yyyy-mm-dd">
	                    {{ Form::text('connection_date', $pay_channel->connection_date, array('class'=>'form-control'), Input::old('connection_date')) }}
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
	                </div>
	            </div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('first_name', 'First Name') }}<br>
					{{ Form::text('first_name', $pay_channel->first_name, array('class'=>'form-control'), Input::old('first_name')) }}
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('last_name', 'Last Name') }}<br>
					{{ Form::text('last_name', $pay_channel->last_name, array('class'=>'form-control'), Input::old('last_name')) }}
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					{{ Form::label('house', 'House') }}<br>
					{{ Form::text('house', $pay_channel->houst, array('class'=>'form-control'), Input::old('house')) }}
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					{{ Form::label('flat', 'Flat') }}<br>
					{{ Form::text('flat', $pay_channel->flat, array('class'=>'form-control'), Input::old('flat')) }}
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					{{ Form::label('road', 'road') }}<br>
					{{ Form::text('road', $pay_channel->road, array('class'=>'form-control'), Input::old('road')) }}
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					{{ Form::label('block', 'block') }}<br>
					{{ Form::text('block', $pay_channel->block, array('class'=>'form-control'), Input::old('block')) }}
				</div>
			</div>

			<div class="col-md-8">
				<div class="form-group">
	                {{ Form::label('area', 'Area') }} <br />
	                {{ Form::select('area', $area, $pay_channel->area, array('class'=>'form-control'), Input::old('area'))}}
	            </div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('phone', 'Phone') }}<br>
					{{ Form::text('phone', $pay_channel->phone, array('class'=>'form-control'), Input::old('phone')) }}
				</div>					
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('mobile', 'Mobile') }}<br>
					{{ Form::text('mobile', $pay_channel->mobile, array('class'=>'form-control'), Input::old('mobile')) }}
				</div>					
			</div>

			{{ Form::hidden('role', 'pay_channel') }}

			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('connection_fee', 'Connection Fee') }}<br>
					{{ Form::text('connection_fee', $pay_channel->connection_fee, array('class'=>'form-control'), Input::old('connection_fee')) }}
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					{{ Form::label('monthly_fee', 'Monthly Fee') }}<br>
					{{ Form::text('monthly_fee', $pay_channel->monthly_fee, array('class'=>'form-control'), Input::old('monthly_fee')) }}
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					{{ Form::label('monthly_vat', 'Monthly Vat') }}<br>
					<div class="col-md-11">
						{{ Form::text('monthly_vat', $pay_channel->monthly_vat, array('class'=>'form-control'), Input::old('monthly_vat')) }}
					</div><div class="col-md-1">%</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					{{ Form::label('additional_fee', 'Additional Fee') }}<br>
					{{ Form::text('additional_fee', $pay_channel->additional_fee, array('class'=>'form-control'), Input::old('additional_fee')) }}
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					{{ Form::label('discount', 'Discount') }}<br>
					{{ Form::text('discount', $pay_channel->discount, array('class'=>'form-control'), Input::old('discount')) }}
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					{{ Form::label('note', 'Note') }}<br>
					{{ Form::text('note', $pay_channel->note, array('class'=>'form-control'), Input::old('note')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('status', 'Active') }}
					@if($pay_channel->status==1)
						{{ Form::hidden('status', false) }}
    					{{ Form::checkbox('status', true) }}Connected
					@else
						{{ Form::hidden('status', false) }}
    					{{ Form::checkbox('status', true) }}Not Connected
					@endif
				</div>	

				<p>{{ Form::submit('Update', array('class'=>'btn btn-danger')) }}</p>
			</div>
			
		{{ Form::close() }}
	</div>
@endsection