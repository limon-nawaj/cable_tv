@extends('commons.default')

@section('content')

	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Pay Channel
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('pay_channel/index') }}">
                Pay Channel
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

	<div class="col-sm-12">
		
	<a href="{{ URL::to('pay_channel/create') }}" class="btn btn-success">Add New pay_channel</a><br>

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
		  <li class="active"><a href="#current_pay_channel" role="tab" data-toggle="tab">Current pay_channel</a></li>
		  <li><a href="#x-pay_channel" role="tab" data-toggle="tab">x-pay_channel</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div class="tab-pane active" id="current_pay_channel">
		  		<table class="table">
					<tr>
						<td>ID</td>
						<td>Name</td>
						<td>Address</td>
						<td>Contact Number</td>
						<td>Monthly fee</td>
						<td>Monthly Vat</td>
						<td>Action</td>
					</tr>

					@foreach($pay_channels as $pay_channel)
						@if($pay_channel->status == 1)
							<tr>
								<td>{{ $pay_channel->chan_id }}</td>
								<td>{{ $pay_channel->first_name }} {{ $pay_channel->last_name }}</td>
								<td>
									@if(empty($pay_channel->block))
										{{ $pay_channel->flat }}, {{ $pay_channel->house }}, {{ $pay_channel->road }}, {{ Area::find($pay_channel->area)->name }}
									@else
										{{ $pay_channel->flat }}, {{ $pay_channel->house }}, {{ $pay_channel->road }}, {{ $pay_channel->block }}, {{ Area::find($pay_channel->area)->name }}
									@endif
								</td>
								<td>
									@if(empty($pay_channel->phone))
										{{ $pay_channel->mobile }}
									@else
										{{ $pay_channel->phone }}, {{ $pay_channel->mobile }}
									@endif
								</td>
								<td>{{ $pay_channel->monthly_fee }}</td>
								<td>{{ $pay_channel->monthly_vat }}</td>
								<td>
									{{ HTML::linkRoute('edit_pay_channel', 'Edit', $pay_channel->id, array('class'=>'btn btn-info btn-sm')) }} | 
									{{ HTML::linkRoute('new_payment', 'Paid', $pay_channel->id, array('class'=>'btn btn-info btn-sm')) }} | 
									{{ HTML::linkRoute('pay_channel', 'Show', $pay_channel->id, array('class'=>'btn btn-info btn-sm')) }}
								</td>
							</tr>
						@endif
					@endforeach
				</table>
		  </div>
		  <div class="tab-pane" id="x-pay_channel">
		  		<table class="table">
					<tr>
						<td>ID</td>
						<td>Name</td>
						<td>Address</td>
						<td>Contact Number</td>
						<td>Monthly fee</td>
						<td>Monthly Vat</td>
						<td>Action</td>
					</tr>

					@foreach($pay_channels as $pay_channel)
						@if($pay_channel->status == 0)
							<tr>
								<td>{{ $pay_channel->id }}</td>
								<td>{{ $pay_channel->first_name }} {{ $pay_channel->last_name }}</td>
								<td>
									@if(empty($pay_channel->block))
										{{ $pay_channel->flat }}, {{ $pay_channel->house }}, {{ $pay_channel->road }}, {{ Area::find($pay_channel->area)->name }}
									@else
										{{ $pay_channel->flat }}, {{ $pay_channel->house }}, {{ $pay_channel->road }}, {{ $pay_channel->block }}, {{ Area::find($pay_channel->area)->name }}
									@endif
								</td>
								<td>
									@if(empty($pay_channel->phone))
										{{ $pay_channel->mobile }}
									@else
										{{ $pay_channel->phone }}, {{ $pay_channel->mobile }}
									@endif
								</td>
								<td>{{ $pay_channel->monthly_fee }}</td>
								<td>{{ $pay_channel->monthly_vat }}</td>
								<td>
									{{ HTML::linkRoute('edit_pay_channel', 'Edit', $pay_channel->id) }} | 
									<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal{{$pay_channel->id}}">
				                      Paid
				                    </button>

				                    <!-- Modal -->
				                    <div class="modal fade" id="myModal{{$pay_channel->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				                      <div class="modal-dialog">
				                        <div class="modal-content">
				                          <div class="modal-header">
				                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				                            <h4 class="modal-title" id="myModalLabel">Get Paid</h4>
				                          </div>
				                          {{ Form::open(array('route'=>'paid_channel_bill', 'method'=>'PUT', 'id'=>$pay_channel->id)) }}
				                          <div class="modal-body">
				                            
				                              {{ Form::hidden('channel_id', $pay_channel->id) }}
				                              {{ Form::label('credit', 'Credit') }}<br>
				                              {{ Form::text('credit', '', array('class'=>'form-control'), Input::old('credit')) }}
				                              
				                            
				                          </div>
				                          <div class="modal-footer">
				                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				                            <!-- <button type="button" onclick="this.form.submit();" class="btn btn-primary">Save</button> -->
				                            {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
				                          </div>
				                          {{ Form::close() }}
				                        </div>
				                      </div>
				                    </div>
				                    <!-- Modal -->
								</td>
							</tr>
						@endif
					@endforeach
				</table>
		  </div>
		</div>
	</div>
@endsection