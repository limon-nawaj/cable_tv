@extends('commons.default')

@section('content')

	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Pay Channel
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('pay_channel/index') }}">
                Pay Channel
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="col-md-7">
      <h2>{{$channel->first_name}} {{$channel->last_name}}</h2>
      <table class="table">
        <tr>
          <th>Month</th>
          <th>Year</th>
         
          <th>Debit</th>
          <th>Credit</th>
          <th>Total</th>
        </tr>

        @foreach($invoices as $invoice)
          <tr>
            <td>{{ $invoice->month }}</td>
            <td>{{ $invoice->year }}</td>
            <td>{{ ChannelBill::where('invoice_id', $invoice->id)->where('credit', 0)->sum('debit') }}</td>
            <td>{{ ChannelBill::where('invoice_id', $invoice->id)->where('debit', 0)->sum('credit') }}</td>
            <td>{{ ChannelBill::where('invoice_id', $invoice->id)->where('debit', 0)->sum('credit')-ChannelBill::where('invoice_id', $invoice->id)->where('credit', 0)->sum('debit') }}</td>
          </tr>
        @endforeach
      </table>
    </div>

    <div class="col-md-5">
      <h3>channel Info</h3>
      <div class="col-md-4">channel Id </div>
      <div class="col-md-8">: {{ $channel->cust_id }}</div>
      <div class="col-md-4">Full Name </div>
      <div class="col-md-8">: {{$channel->first_name}} {{$channel->last_name}}</div>

      <div class="col-md-4">Address </div>
      <div class="col-md-8">
        @if(empty($channel->block))
          : {{ $channel->flat }}, {{ $channel->house }}, {{ $channel->road }}
        @else
          : {{ $channel->flat }}, {{ $channel->house }}, {{ $channel->road }}, {{ $channel->block }}
        @endif
      </div>

      <div class="col-md-4">Phone </div>
      <div class="col-md-8">: {{ $channel->phone }}</div>

      <div class="col-md-4">Mobile </div>
      <div class="col-md-8">: {{ $channel->mobile }}</div>

      <div class="col-md-4">Join Date </div>
      <div class="col-md-8">: {{ $channel->connection_date }}</div>

      <div class="col-md-4">Connection Fee </div>
      <div class="col-md-8">: {{ $channel->connection_fee }}</div>

      <div class="col-md-4">Total TV </div>
      <div class="col-md-8">: {{ $channel->num_of_tv }}</div>

      <div class="col-md-4">Monthly Fee </div>
      <div class="col-md-8">: {{ $channel->monthly_fee }}</div>

      <div class="col-md-4">Status </div>
      <div class="col-md-8">
        @if($channel->status == 1)
          : Connected
        @else
          : Not Connected
        @endif
      </div>

      <div class="col-md-4">Note </div>
      <div class="col-md-8">: {{ $channel->note }}</div>

    </div>


@endsection