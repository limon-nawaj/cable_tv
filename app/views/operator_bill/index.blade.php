@extends('commons.default')

@section('content')
  <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Bill <small></small>
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('bill/index') }}">
                Bill
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
     <!-- Month and year field start -->
       
        <div class="row">
          <div class="col-md-8">
            {{ Form::open(array('route'=>'operator_search', 'method'=>'GET')) }}
              <div class="form-group">
                <div class="col-md-2">{{ Form::label('area', 'Area') }}</div>
                <div class="col-md-8">{{ Form::select('area', $area, '', array('class'=>'form-control'), Input::old('area'))}}</div>
                <div class="col-md-2">{{ Form::submit('Search', array('class'=>'btn green')) }}</div>
              </div>
            {{ Form::close() }}
          </div>
        </div>
        
    <!-- Month and year field end -->

  

    <div class="col-md-12">
      <div class="row">
        <table class="table">
            <tr>
              <th>Operator ID</th>
              <th>Operator Name</th> 
              <th>Additional Fee</th>
              <th>Debit</th>
              <th>Credit</th>
              <th>Total Bill</th>
              <th>Action</th>
              <th></th>
            </tr>

            @foreach ($operator_invoices as $operator_invoice)
              
                <?php $operator = SubOperator::find($operator_invoice->operator_id) ?>
                <tr>
                  <td>{{ $operator->opt_id }}</td>
                  <td>{{ $operator->first_name }} {{ $operator->last_name }}</td>              
                  

                  <td>
                    {{ $operator_invoice->additional_fee }}
                  </td>

                  <?php $b = OperatorBill::where('operator_id', $operator->id)->orderBy('id', 'desc')->first() ?>

                  <td>
                    {{ OperatorBill::where('operator_id', $operator->id)->where('credit', 0)->orderBy('id', 'desc')->first()->debit }}
                  </td>

                  <td>
                    {{ OperatorBill::where('invoice_id', $b->invoice_id)->sum('credit') }}
                  </td>

                  <td>
                    {{ $b->total }}
                  </td>

                  <!-- <td>
                    {{ HTML::linkRoute('edit_bill', 'Edit', $b->id) }}
                  </td> -->

                  <td>
                    
                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal{{$operator_invoice->id}}">
                      Paid
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="myModal{{$operator_invoice->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Get Paid</h4>
                          </div>
                          {{ Form::open(array('route'=>'paid_operator_bill', 'method'=>'PUT', 'id'=>$operator_invoice->id)) }}
                          <div class="modal-body">
                            
                              {{ Form::hidden('invoice_id', $b->invoice_id) }}
                              {{ Form::hidden('operator_id', $operator->id) }}
                              {{ Form::label('credit', 'Credit') }}<br>
                              {{ Form::text('credit', '', array('class'=>'form-control'), Input::old('credit')) }}
                              
                            
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <!-- <button type="button" onclick="this.form.submit();" class="btn btn-primary">Save</button> -->
                            {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
                          </div>
                          {{ Form::close() }}
                        </div>
                      </div>
                    </div>
                    <!-- Modal -->
                  </td>
                </tr> 
            @endforeach
          </table>
      </div>

    </div>

    

@endsection