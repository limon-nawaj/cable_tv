@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Operator Invoice <small></small>
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('operator_invoice/index') }}">
                Operator Invoices
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

    <div class="row">
      <div class="col-md-6">
        
        {{ Form::open(array('route'=>'new_operator_invoice', 'method'=>'GET')) }}
          <div class="form-group">
            <div class="col-md-2">{{ Form::label('area', 'Area') }}</div>
            <div class="col-md-8">{{ Form::select('area', $area, '', array('class'=>'form-control'), Input::old('area'))}}</div>
            <div class="col-md-2">{{ Form::submit('Search', array('class'=>'btn green', 'id'=>'mySUBMIT')) }}</div>
          </div>
        {{ Form::close() }}
      </div>
    </div>

    <script type="text/javascript">
      $('#mySUBMIT').bind('click', function() {
          var blanks = $('form').find('select,input:not(:button)').filter(function () {
               return ($(this).val() == '');
          });
          if (blanks && blanks.length > 0) { alert("All fields MUST be filled out/selected!"); }
          else { $("form").submit(); }
      });
    </script>

    <!-- <div class="row">
      <div class="col-md-6">
        
        {{ Form::open(array('route'=>'new_operator_invoice', 'method'=>'GET')) }}
          <div class="form-group">
            <div class="col-md-2">{{ Form::label('area', 'Area') }}</div>
            <div class="col-md-8">{{ Form::select('area', $area, '', array('class'=>'form-control'), Input::old('area'))}}</div>
            <div class="col-md-2">{{ Form::submit('Search', array('class'=>'btn green')) }}</div>
          </div>
        {{ Form::close() }}
      </div>
    </div> -->

    <!-- @if($sub_operators->count())
      {{ Form::open(array('route'=>'save_operator_invoice', 'method'=>'POST')) }}
        <div class="row">
          <div class="col-md-12">
          {{ Form::hidden('type', 'monthly') }}
            <div class="col-md-5">
              <div class="form-group">
                <div class="col-md-2">
                  {{ Form::label('month', 'Month') }}
                </div>
                <div class="col-md-10">
                  {{ Form::select('month', $month, '', array('class'=>'form-control'), Input::old('month')) }}                  
                </div>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <div class="col-md-2">
                  {{ Form::label('year', 'Year') }}
                </div>
                <div class="col-md-9">
                  {{ Form::select('year', $year, '', array('class'=>'form-control'), Input::old('year')) }}
                </div>
              </div>
            </div>
            <div class="col-md-2">
              {{ Form::submit('Generate Invoice', array('class'=>'btn green')) }}
            </div>
          </div>
        </div>
      {{ Form::close() }} -->  

      <!-- <div class="row">
        <div class="col-md-12">
          <h2>Sub Operator List</h2>
            <table class="table">
              <tr>
                <th>CID</th>
                <th>Name</th>
                <th>Number of TV</th>
                <th>Monthly Fee</th>
                <th>Vat</th>
              </tr>

              @foreach($sub_operators as $sub_operator)
                @if($sub_operator->status == 1)
                  <tr>
                    <td>ID</td>
                    <td>{{ $sub_operator->first_name }} {{ $sub_operator->last_name }}</td>
                    <td>{{ $sub_operator->num_of_tv }}</td>
                    <td>{{ $sub_operator->monthly_fee }}</td>
                    <td>{{ $sub_operator->monthly_vat }}%</td>
                  </tr>
                @endif
              @endforeach
            </table>
        </div>
      </div>
    @endif -->

    
@endsection