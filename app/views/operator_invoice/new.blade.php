@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Operator Invoice <small></small>
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('operator_invoice/index') }}">
                Operator Invoices
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->


    @if($sub_operators->count())
      {{ Form::open(array('route'=>'save_operator_invoice', 'method'=>'POST')) }}
        <!-- Month and year field start -->
        <div class="row">
          <div class="col-md-12">
          {{ Form::hidden('area_id', $area_id) }}
          {{ Form::hidden('type', 'monthly') }}
            <div class="col-md-4">
              <div class="form-group">
                <div class="col-md-2">
                  {{ Form::label('month', 'Month') }}
                </div>
                <div class="col-md-10">
                  {{ Form::select('month', $month, '', array('class'=>'form-control'), Input::old('month')) }}                  
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <div class="col-md-2">
                  {{ Form::label('year', 'Year') }}
                </div>
                <div class="col-md-9">
                  {{ Form::select('year', $year, '', array('class'=>'form-control'), Input::old('year')) }}
                </div>
              </div>
            </div>
            {{ Form::hidden('type', 'monthly') }}
            <!-- <div class="col-md-4">
              <div class="form-group">
                <div class="col-md-2">
                  {{ Form::label('type', 'Type') }}
                </div>
                <div class="col-md-9">
                  {{ Form::select('type', $type, '', array('class'=>'form-control'), Input::old('type')) }}
                </div>
              </div>
            </div> -->
            
          </div>
        </div>
        <!-- Month and year field end -->
      

        <div class="row">
          <div class="col-md-12">
          	<h2>Sub Operator List
            @if(!empty($area_id))
              of {{ Area::find($area_id)->name }}
            @endif
            </h2>
              <table class="table">
                <tr>
                  <th>#</th>
                  <th>CID</th>
                  <th>Name</th>
                  <th>Number of TV</th>
                  <th>Monthly Fee</th>
                  <th>Vat</th>
                </tr>

                @foreach($sub_operators as $sub_operator)
                  @if($sub_operator->status == 1)
                  	<tr>
  	                  <td>
                          <input type="checkbox" name="formOperator[]" value="{{$sub_operator->id}}" />
                      </td>
                      <td>{{ $sub_operator->opt_id }}</td>
  	                  <td>{{ $sub_operator->first_name }} {{ $sub_operator->last_name }}</td>
  	                  <td>{{ $sub_operator->num_of_tv }}</td>
                      <td>{{ $sub_operator->monthly_fee }}</td>
  	                  <td>{{ $sub_operator->monthly_vat }}%</td>
  	                </tr>
                  @endif
                @endforeach
              </table>
          </div>
        </div>
        <div class="pull-right">
          {{ Form::submit('Generate Invoice', array('class'=>'btn green', 'name'=>'formSubmit')) }}
        </div>
      {{ Form::close() }}  
    @endif
@endsection