@extends('commons.default')

@section('content')
	
  <!-- Start Customer -->
  @if(!empty($customer))
    <div class="col-md-7">
      <h2>{{$customer->first_name}} {{$customer->last_name}}</h2>
      <table class="table">
        <tr>
          <th>Month</th>
          <th>Year</th>
          <th>Area</th>
          
          <th>Debit</th>
          <th>Credit</th>
          <th>Total</th>
        </tr>

        @foreach($invoices as $invoice)
          <tr>
            <td>{{ $invoice->month }}</td>
            <td>{{ $invoice->year }}</td>
            <td>{{ Area::find($invoice->area_id)->name }}</td>
            <td>{{ Bill::where('invoice_id', $invoice->id)->where('credit', 0)->sum('debit') }}</td>
            <td>{{ Bill::where('invoice_id', $invoice->id)->where('debit', 0)->sum('credit') }}</td>
            <td>{{ Bill::where('invoice_id', $invoice->id)->where('debit', 0)->sum('credit')-Bill::where('invoice_id', $invoice->id)->where('credit', 0)->sum('debit') }}</td>
          </tr>
        @endforeach
      </table>
    </div>

    <div class="col-md-5">
      <button class="btn btn-info" data-toggle="modal" data-target="#myModal">Paid</button>
      <h3>Customer Info</h3>
      <div class="col-md-4">Customer Id </div>
      <div class="col-md-8">: {{ $customer->cust_id }}</div>
      <div class="col-md-4">Full Name </div>
      <div class="col-md-8">: {{$customer->first_name}} {{$customer->last_name}}</div>

      <div class="col-md-4">Address </div>
      <div class="col-md-8">
        @if(empty($customer->block))
          : {{ $customer->flat }}, {{ $customer->house }}, {{ $customer->road }}, {{ Area::find($customer->area)->name }}
        @else
          : {{ $customer->flat }}, {{ $customer->house }}, {{ $customer->road }}, {{ $customer->block }}, {{ Area::find($customer->area)->name }}
        @endif
      </div>

      <div class="col-md-4">Phone </div>
      <div class="col-md-8">: {{ $customer->phone }}</div>

      <div class="col-md-4">Mobile </div>
      <div class="col-md-8">: {{ $customer->mobile }}</div>

      <div class="col-md-4">Join Date </div>
      <div class="col-md-8">: {{ $customer->connection_date }}</div>

      <div class="col-md-4">Connection Fee </div>
      <div class="col-md-8">: {{ $customer->connection_fee }}</div>

      <div class="col-md-4">Total TV </div>
      <div class="col-md-8">: {{ $customer->num_of_tv }}</div>

      <div class="col-md-4">Monthly Fee </div>
      <div class="col-md-8">: {{ $customer->monthly_fee }}</div>

      <div class="col-md-4">Status </div>
      <div class="col-md-8">
        @if($customer->status == 1)
          : Connected
        @else
          : Not Connected
        @endif
      </div>

      <div class="col-md-4">Note </div>
      <div class="col-md-8">: {{ $customer->note }}</div>

    </div>

    <?php $b = Bill::where('customer_id', $customer->id)->orderBy('id', 'desc')->first() ?>

    <!-- get paid -->
    <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel">Get Paid</h4>
            </div>
            {{ Form::open(array('route'=>'paid_bill', 'method'=>'PUT', 'id'=>$invoice->id)) }}
            <div class="modal-body">
              
                {{ Form::hidden('invoice_id', $b->invoice_id) }}
                {{ Form::hidden('customer_id', $customer->id) }}
                {{ Form::label('credit', 'Credit') }}<br>
                {{ Form::text('credit', '', array('class'=>'form-control'), Input::old('credit')) }}
                
                {{ Form::hidden('page', 'individual') }}
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <!-- <button type="button" onclick="this.form.submit();" class="btn btn-primary">Save</button> -->
              {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
            </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
      <!-- Modal -->
    <!-- get paid -->

  @endif
  <!-- End Customer -->

  <!-- Start Operator -->
  @if(!empty($operator))
    <div class="col-md-7">
      <h2>{{$sub_operator->first_name}} {{$sub_operator->last_name}}</h2>
      <table class="table">
        <tr>
          <th>Month</th>
          <th>Year</th>
          <th>Area</th>
          <th>Debit</th>
          <th>Credit</th>
          <th>Total</th>
        </tr>

        @foreach($operator_invoices as $operator_invoice)
          <tr>
            <td>{{ $operator_invoice->month }}</td>
            <td>{{ $operator_invoice->year }}</td>
            <td>{{ Area::find($operator_invoice->area_id)->name }}</td>
            <td>{{ OperatorBill::where('invoice_id', $operator_invoice->id)->where('credit', 0)->sum('debit') }}</td>
            <td>{{ OperatorBill::where('invoice_id', $operator_invoice->id)->where('debit', 0)->sum('credit') }}</td>
            <td>{{ OperatorBill::where('invoice_id', $operator_invoice->id)->where('debit', 0)->sum('credit')-Bill::where('invoice_id', $operator_invoice->id)->where('credit', 0)->sum('debit') }}</td>
          </tr>
        @endforeach
      </table>
    </div>

    <div class="col-md-5">
      <button class="btn btn-info" data-toggle="modal" data-target="#myModal">Paid</button>
      <h3>Sub-Operator Info</h3>
      <div class="col-md-4">Sub-Operator Id </div>
      <div class="col-md-8">: {{ $sub_operator->opt_id }}</div>
      <div class="col-md-4">Full Name </div>
      <div class="col-md-8">: {{$sub_operator->first_name}} {{$sub_operator->last_name}}</div>

      <div class="col-md-4">Address </div>
      <div class="col-md-8">
        @if(empty($sub_operator->block))
          : {{ $sub_operator->flat }}, {{ $sub_operator->house }}, {{ $sub_operator->road }}, {{ Area::find($sub_operator->area)->name }}
        @else
          : {{ $sub_operator->flat }}, {{ $sub_operator->house }}, {{ $sub_operator->road }}, {{ $sub_operator->block }}, {{ Area::find($sub_operator->area)->name }}
        @endif
      </div>

      <div class="col-md-4">Phone </div>
      <div class="col-md-8">: {{ $sub_operator->phone }}</div>

      <div class="col-md-4">Mobile </div>
      <div class="col-md-8">: {{ $sub_operator->mobile }}</div>

      <div class="col-md-4">Join Date </div>
      <div class="col-md-8">: {{ $sub_operator->connection_date }}</div>

      <div class="col-md-4">Connection Fee </div>
      <div class="col-md-8">: {{ $sub_operator->connection_fee }}</div>

      <div class="col-md-4">Total TV </div>
      <div class="col-md-8">: {{ $sub_operator->num_of_tv }}</div>

      <div class="col-md-4">Monthly Fee </div>
      <div class="col-md-8">: {{ $sub_operator->monthly_fee }}</div>

      <div class="col-md-4">Status </div>
      <div class="col-md-8">
        @if($sub_operator->status == 1)
          : Connected
        @else
          : Not Connected
        @endif
      </div>

      <div class="col-md-4">Note </div>
      <div class="col-md-8">: {{ $sub_operator->note }}</div>

    </div>

    <?php $b = OperatorBill::where('operator_id', $sub_operator->id)->orderBy('id', 'desc')->first() ?>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Get Paid</h4>
          </div>
          {{ Form::open(array('route'=>'paid_operator_bill', 'method'=>'PUT', 'id'=>$operator_invoice->id)) }}
          <div class="modal-body">
            
              {{ Form::hidden('invoice_id', $b->invoice_id) }}
              {{ Form::hidden('operator_id', $sub_operator->id) }}
              {{ Form::label('credit', 'Credit') }}<br>
              {{ Form::text('credit', '', array('class'=>'form-control'), Input::old('credit')) }}
              

              {{ Form::hidden('page', 'individual') }}
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" onclick="this.form.submit();" class="btn btn-primary">Save</button> -->
            {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
    <!-- Modal -->

  @endif  
  <!-- End Operator -->

  <!-- Start Pay Channel -->
  @if(!empty($channel))
    <div class="col-md-7">

      <table class="table">
        <tr>
          <th>Date</th>
          <th>Type</th>
          <th>Credit</th>
        </tr>

        @foreach($bills as $bill)
          <tr>
            <td>{{ $bill->payment_date }}</td>
            <td>{{ $bill->type }}</td>
            <td>{{ $bill->credit }}</td>
          </tr>
        @endforeach
      </table>
    </div>

    <div class="well col-md-5">
        <div class="col-md-4">Name</div>
        <div class="col-md-8">: {{ $pay_channel->first_name }} {{ $pay_channel->last_name }}</div>

        <div class="col-md-4">Address</div>
        <div class="col-md-8">
          @if(empty($pay_channel->block))
            : {{ $pay_channel->flat }}, {{ $pay_channel->house }}, {{ $pay_channel->road }}, {{ Area::find($pay_channel->area)->name }}
          @else
            : {{ $pay_channel->flat }}, {{ $pay_channel->house }}, {{ $pay_channel->road }}, {{ $pay_channel->block }}, {{ Area::find($pay_channel->area)->name }}
          @endif
        </div>

        <div class="col-md-4">Phone</div>
        <div class="col-md-8">{{ $pay_channel->phone }}</div>

        <div class="col-md-4">Mobile</div>
        <div class="col-md-8">: {{ $pay_channel->mobile }}</div>

        <div class="col-md-4">Connection Date</div>
        <div class="col-md-8">: {{ $pay_channel->connection_date }}</div>

        <div class="col-md-4">Connection Fee</div>
        <div class="col-md-8">: {{ $pay_channel->connection_fee }}</div>

        <div class="col-md-4">Number of TV</div>
        <div class="col-md-8">: {{ $pay_channel->num_of_tv }}</div>

        <div class="col-md-4">Monthly Vat</div>
        <div class="col-md-8">: {{ $pay_channel->monthly_vat }}</div>

        <div class="col-md-4">Note</div>
        <div class="col-md-8">: {{ $pay_channel->note }}</div>
    </div>
  @endif
  <!-- End Pay Channel -->
@endsection