@extends('commons.default')

@section('content')
	<!-- All due (All customer/Sub-Operator/Pay channel/Employee) -->
	<div class="row">
      <div class="col-md-6">
        
        {{ Form::open(array('route'=>'all_due', 'method'=>'GET')) }}
          <div class="form-group">
            <div class="col-md-8">{{ Form::select('user_type', $user_type, '', array('class'=>'form-control')) }}</div>
            <div class="col-md-2">{{ Form::submit('All Due', array('class'=>'btn green')) }}</div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
    <!-- All due (All customer/Sub-Operator/Pay channel/Employee) -->

    <!-- All paid (All customer/Sub-Operator/Pay channel/Employee) -->
	<div class="row">
      <div class="col-md-6">
        
        {{ Form::open(array('route'=>'all_paid', 'method'=>'GET')) }}
          <div class="form-group">
            <div class="col-md-8">{{ Form::select('user_type', $user_type, '', array('class'=>'form-control')) }}</div>
            <div class="col-md-2">{{ Form::submit('All Paid', array('class'=>'btn green')) }}</div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
    <!-- All paid (All customer/Sub-Operator/Pay channel/Employee) -->

    <!-- Current month due (All customer/Sub-Operator/Pay channel) -->
	<div class="row">
      <div class="col-md-6">
        
        {{ Form::open(array('route'=>'current_month_due', 'method'=>'GET')) }}
          <div class="form-group">
            <div class="col-md-8">{{ Form::select('user_type', $user_type, '', array('class'=>'form-control')) }}</div>
            <div class="col-md-2">{{ Form::submit('Current Month Due', array('class'=>'btn green')) }}</div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
    <!-- Current month due (All customer/Sub-Operator/Pay channel) -->

    <!-- Current month due (Individual customer/Sub-Operator/Pay channel) -->
    <div class="row">
    	<div class="col-md-8">
        
        {{ Form::open(array('route'=>'current_month_individual_due', 'method'=>'GET')) }}
          <div class="form-group">
            <div class="col-md-6">{{ Form::select('user_type', $user_type, '', array('class'=>'form-control')) }}</div>
            <div class="col-md-4">{{ Form::text('id', '', array('class'=>'form-control', 'placeholder'=>'0000'))}}</div>
            <div class="col-md-2">{{ Form::submit('Current Month Due', array('class'=>'btn green')) }}</div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
    <!-- Current month due (Individual customer/Sub-Operator/Pay channel) -->

    <!-- Individual due and paid (Individual customer/Sub-Operator/Pay channel) -->
    <div class="row">
    	<div class="col-md-8">
        
        {{ Form::open(array('route'=>'individual_paid_due', 'method'=>'GET')) }}
          <div class="form-group">
            <div class="col-md-6">{{ Form::select('user_type', $user_type, '', array('class'=>'form-control')) }}</div>
            <div class="col-md-4">{{ Form::text('id', '', array('class'=>'form-control', 'placeholder'=>'0000'))}}</div>
            <div class="col-md-2">{{ Form::submit('Individual Due', array('class'=>'btn green')) }}</div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
    <!-- Individual due and paid (Individual customer/Sub-Operator/Pay channel) -->

    <!-- Date Range Due -->
    <div class="row">
      <div class="col-md-10">
        
        {{ Form::open(array('route'=>'date_range_due', 'method'=>'GET')) }}
          <div class="form-group">
            <div class="col-md-4">{{ Form::select('user_type', $user_type, '', array('class'=>'form-control')) }}</div>
            <div class="col-md-3">
                <div class='input-group date' id="datetimepicker5" data-date-format="yyyy-mm-dd">
                    {{ Form::text('start_date', '', array('class'=>'form-control', 'placeholder'=>'Start Date')) }}
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class='input-group date' id="datetimepicker6" data-date-format="yyyy-mm-dd">
                    {{ Form::text('end_date', '', array('class'=>'form-control', 'placeholder'=>'End Date')) }}
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </div>
            </div>
            <div class="col-md-2">{{ Form::submit('All Due', array('class'=>'btn green')) }}</div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
    <!-- Date Range Due -->

    <!-- Date Range Paid -->
    <div class="row">
      <div class="col-md-10">
        
        {{ Form::open(array('route'=>'date_range_paid', 'method'=>'GET')) }}
          <div class="form-group">
            <div class="col-md-4">{{ Form::select('user_type', $user_type, '', array('class'=>'form-control')) }}</div>
            <div class="col-md-3">
                <div class='input-group date' id="datetimepicker1" data-date-format="yyyy-mm-dd">
                    {{ Form::text('start_date', '', array('class'=>'form-control', 'placeholder'=>'Start Date')) }}
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class='input-group date' id="datetimepicker2" data-date-format="yyyy-mm-dd">
                    {{ Form::text('end_date', '', array('class'=>'form-control', 'placeholder'=>'End Date')) }}
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </div>
            </div>
            <div class="col-md-2">{{ Form::submit('All Paid', array('class'=>'btn green')) }}</div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
    <!-- Date Range Paid -->
@endsection