@extends('commons.default')

@section('content')
  <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          All Due <small></small>
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('reports/index') }}">
                Reports
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    
    <!-- customer table -->
    @if(!empty($customers))
      <div class="col-md-12">
        <div class="row">
          <table class="table">
              <tr>
                <th>Customer ID</th>
                <th>Customer Name</th> 
                <th>Debit</th>
                <th>Credit</th>
                <th>Total Bill</th>
              </tr>



              @foreach ($customers as $customer)
                  <?php $bill = Bill::where('customer_id', $customer->id)->whereBetween('created_at', array(date("Y-m-d", strtotime($startDate)), date("Y-m-d", strtotime($endDate))))->orderBy('id', 'desc')->first() ?>
                  <?php if (!empty($bill)): ?>
                    @if($bill->total>0)
                        <tr>
                          <td>{{$customer->cust_id}}</td>
                          <td>{{ $customer->first_name }} {{ $customer->last_name }}</td>              
                          
                          <td>
                            {{ Bill::where('customer_id', $customer->id)->where('credit', 0)->orderBy('id', 'desc')->first()->debit }}
                          </td>

                          <td>
                            {{ Bill::where('invoice_id', $bill->invoice_id)->sum('credit') }}
                          </td>

                          <td>
                            {{ $bill->total }}
                          </td>

                          
                        </tr> 
                    @endif
                  <?php endif ?>
                      
               
              @endforeach
            </table>
        </div>
      </div>
    @endif
    <!-- customer table -->


    <!-- operator table -->
    @if(!empty($operators))
      <div class="col-md-12">
        <div class="row">
          <table class="table">
              <tr>
                <th>Sub-Operator ID</th>
                <th>Sub-Operator Name</th> 
                <th>Debit</th>
                <th>Credit</th>
                <th>Total Bill</th>
              </tr>

              @foreach ($operators as $operator)
                  <?php $bill = OperatorBill::where('operator_id', $operator->id)->whereBetween('created_at', array(date("Y-m-d", strtotime($startDate)), date("Y-m-d", strtotime($endDate))))->orderBy('id', 'desc')->first() ?>
                    <?php if (!empty($bill)): ?>
                      @if($bill->total>0)
                        <tr>
                          <td>{{$operator->opt_id}}</td>
                          <td>{{ $operator->first_name }} {{ $operator->last_name }}</td>              
                          
                          <td>
                            {{ OperatorBill::where('operator_id', $operator->id)->where('credit', 0)->orderBy('id', 'desc')->first()->debit }}
                          </td>

                          <td>
                            {{ OperatorBill::where('invoice_id', $bill->invoice_id)->sum('credit') }}
                          </td>

                          <td>
                            {{ $bill->total }}
                          </td>
                        </tr> 
                      @endif
                  <?php endif ?>
              @endforeach
            </table>
        </div>
      </div>
    @endif
    <!-- operator table -->

    <!-- channel table -->
    @if(!empty($channels))
      <div class="col-md-12">
        <div class="row">
          <table class="table">
              <tr>
                <th>Pay Channel ID</th>
                <th>Pay Channel Name</th> 
                <th>Debit</th>
                <th>Credit</th>
                <th>Total Bill</th>
              </tr>

              @foreach ($channels as $channel)
                  <?php $bill = ChannelBill::where('channel_id', $channel->id)->whereBetween('created_at', array(date("Y-m-d", strtotime($startDate)), date("Y-m-d", strtotime($endDate))))->orderBy('id', 'desc')->first() ?>
                    <?php if (!empty($bill)): ?>
                      @if($bill->total>0)
                        <tr>
                          <td>{{$channel->chan_id}}</td>
                          <td>{{ $channel->first_name }} {{ $channel->last_name }}</td>              
                          
                          <td>
                            {{ ChannelBill::where('channel_id', $channel->id)->where('credit', 0)->orderBy('id', 'desc')->first()->debit }}
                          </td>

                          <td>
                            {{ ChannelBill::where('invoice_id', $bill->invoice_id)->sum('credit') }}
                          </td>

                          <td>
                            {{ $bill->total }}
                          </td>

                          
                        </tr> 
                      @endif
                  <?php endif ?>
              @endforeach
            </table>
        </div>
      </div>
    @endif
    <!-- channel table -->
@endsection