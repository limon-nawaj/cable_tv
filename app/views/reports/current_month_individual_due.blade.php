@extends('commons.default')

@section('content')
  <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Current Month Due <small></small>
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('/') }}">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="{{ URL::to('reports/index') }}">
                Reports
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    
    <!-- customer -->
    @if(!empty($customer))

      <div class="col-md-7">

        <?php $bill = Bill::where('customer_id', $customer->id)->orderBy('id', 'desc')->first() ?>
        <?php if (date("m")==date("m",strtotime($bill->created_at))): ?>
            @if($bill->total<0)
              <div class="col-md-4">Debit</div>
              <div class="col-md-8">
                {{ Bill::where('customer_id', $customer->id)->where('credit', 0)->orderBy('id', 'desc')->first()->debit }}
              </div>

              <div class="col-md-4">Credit</div>
              <div class="col-md-8">
                {{ Bill::where('invoice_id', $bill->invoice_id)->sum('credit') }}
              </div>

              <div class="col-md-4">Total Bill</div>
              <div class="col-md-8">{{ $bill->total }}</div>
            @else
              <h2>No due exist</h2>
            @endif
        <?php else: ?>
          <h2>No bill found.</h2>
        <?php endif ?>
      </div>

      <div class="col-md-5">
      <h3>Customer Info</h3>
      <div class="col-md-4">Customer Id </div>
      <div class="col-md-8">: {{ $customer->cust_id }}</div>
      <div class="col-md-4">Full Name </div>
      <div class="col-md-8">: {{$customer->first_name}} {{$customer->last_name}}</div>

      <div class="col-md-4">Address </div>
      <div class="col-md-8">
        @if(empty($customer->block))
          : {{ $customer->flat }}, {{ $customer->house }}, {{ $customer->road }}, {{ Area::find($customer->area)->name }}
        @else
          : {{ $customer->flat }}, {{ $customer->house }}, {{ $customer->road }}, {{ $customer->block }}, {{ Area::find($customer->area)->name }}
        @endif
      </div>

      <div class="col-md-4">Phone </div>
      <div class="col-md-8">: {{ $customer->phone }}</div>

      <div class="col-md-4">Mobile </div>
      <div class="col-md-8">: {{ $customer->mobile }}</div>

      <div class="col-md-4">Join Date </div>
      <div class="col-md-8">: {{ $customer->connection_date }}</div>

      <div class="col-md-4">Connection Fee </div>
      <div class="col-md-8">: {{ $customer->connection_fee }}</div>

      <div class="col-md-4">Total TV </div>
      <div class="col-md-8">: {{ $customer->num_of_tv }}</div>

      <div class="col-md-4">Monthly Fee </div>
      <div class="col-md-8">: {{ $customer->monthly_fee }}</div>

      <div class="col-md-4">Status </div>
      <div class="col-md-8">
        @if($customer->status == 1)
          : Connected
        @else
          : Not Connected
        @endif
      </div>

      <div class="col-md-4">Note </div>
      <div class="col-md-8">: {{ $customer->note }}</div>

    </div>
  @endif
  <!-- customer -->

  <!-- operator -->
    @if(!empty($operator))

      <div class="col-md-7">
        <div class="col-md-4">Sub-Operator ID</div>
        <div class="col-md-8">{{$operator->cust_id}}</div>

        <div class="col-md-4">Sub-Operator Name</div>
        <div class="col-md-8">{{ $operator->first_name }} {{ $operator->last_name }}</div>
        <?php $bill = OperatorBill::where('operator_id', $operator->id)->orderBy('id', 'desc')->first() ?>
        <?php if (date("m")==date("m",strtotime($bill->created_at))): ?>
            @if($bill->total<0)
              <div class="col-md-4">Debit</div>
              <div class="col-md-8">
                {{ OperatorBill::where('operator_id', $operator->id)->where('credit', 0)->orderBy('id', 'desc')->first()->debit }}
              </div>

              <div class="col-md-4">Credit</div>
              <div class="col-md-8">
                {{ OperatorBill::where('invoice_id', $bill->invoice_id)->sum('credit') }}
              </div>

              <div class="col-md-4">Total Bill</div>
              <div class="col-md-8">{{ $bill->total }}</div>
            @else
              <h3>No due payment.</h3>
            @endif
        <?php else: ?>
            <h2>No bill found.</h2>
        <?php endif ?>
      </div>

      <div class="col-md-5">
      <button class="btn btn-primary" data-toggle="modal" data-target="#invoice">Generate Invoice</button>
      <button class="btn btn-info" data-toggle="modal" data-target="#myModal">Paid</button>
      <h3>Sub-Operator Info</h3>
      <div class="col-md-4">Sub-Operator Id </div>
      <div class="col-md-8">: {{ $sub_operator->opt_id }}</div>
      <div class="col-md-4">Full Name </div>
      <div class="col-md-8">: {{$sub_operator->first_name}} {{$sub_operator->last_name}}</div>

      <div class="col-md-4">Address </div>
      <div class="col-md-8">
        @if(empty($sub_operator->block))
          : {{ $sub_operator->flat }}, {{ $sub_operator->house }}, {{ $sub_operator->road }}, {{ Area::find($sub_operator->area)->name }}
        @else
          : {{ $sub_operator->flat }}, {{ $sub_operator->house }}, {{ $sub_operator->road }}, {{ $sub_operator->block }}, {{ Area::find($sub_operator->area)->name }}
        @endif
      </div>

      <div class="col-md-4">Phone </div>
      <div class="col-md-8">: {{ $sub_operator->phone }}</div>

      <div class="col-md-4">Mobile </div>
      <div class="col-md-8">: {{ $sub_operator->mobile }}</div>

      <div class="col-md-4">Join Date </div>
      <div class="col-md-8">: {{ $sub_operator->connection_date }}</div>

      <div class="col-md-4">Connection Fee </div>
      <div class="col-md-8">: {{ $sub_operator->connection_fee }}</div>

      <div class="col-md-4">Total TV </div>
      <div class="col-md-8">: {{ $sub_operator->num_of_tv }}</div>

      <div class="col-md-4">Monthly Fee </div>
      <div class="col-md-8">: {{ $sub_operator->monthly_fee }}</div>

      <div class="col-md-4">Status </div>
      <div class="col-md-8">
        @if($sub_operator->status == 1)
          : Connected
        @else
          : Not Connected
        @endif
      </div>

      <div class="col-md-4">Note </div>
      <div class="col-md-8">: {{ $sub_operator->note }}</div>

    </div>
  @endif
  <!-- operator -->

  <!-- channel -->
    @if(!empty($channel))

      <div class="col-md-7">
        <div class="col-md-4">Pay Channel ID</div>
        <div class="col-md-8">{{$channel->chan_id}}</div>

        <div class="col-md-4">Pay Channel Name</div>
        <div class="col-md-8">{{ $channel->first_name }} {{ $channel->last_name }}</div>
        <?php $bill = ChannelBill::where('channel_id', $channel->id)->orderBy('id', 'desc')->first() ?>
        <?php if (date("m")==date("m",strtotime($bill->created_at))): ?>
            @if($bill->total<0)
              <div class="col-md-4">Debit</div>
              <div class="col-md-8">
                {{ ChannelBill::where('channel_id', $channel->id)->where('credit', 0)->orderBy('id', 'desc')->first()->debit }}
              </div>

              <div class="col-md-4">Credit</div>
              <div class="col-md-8">
                {{ ChannelBill::where('invoice_id', $bill->invoice_id)->sum('credit') }}
              </div>

              <div class="col-md-4">Total Bill</div>
              <div class="col-md-8">{{ $bill->total }}</div>
            @else
              <h2>No due found</h2>
            @endif
        <?php else: ?>
          <h2>No bill found.</h2>
        <?php endif ?>
      </div>
  @endif
  <!-- channel -->

  @if(empty($customer) && empty($operator) && empty($channel))
    <h3>No data is found.</h3>
  @endif  
      
@endsection