<!DOCTYPE html>
<html lang="en">
  <head>
  	<meta charset="UTF-8"/>
    <title></title>
    {{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') }}
    {{ HTML::style('plugins/font-awesome/css/font-awesome.min.css') }}
    {{ HTML::style('css/style.css') }}
    {{ HTML::style('plugins/bootstrap/css/bootstrap.min.css') }}
    {{ HTML::style('css/bootstrap-datetimepicker.css') }}
    {{ HTML::style('css/bootstrap-datetimepicker.min.css') }}

    
    {{ HTML::style('plugins/uniform/css/uniform.default.css') }}


    <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
    {{ HTML::style('plugins/gritter/css/jquery.gritter.css') }}
    {{ HTML::style('plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}
    {{ HTML::style('plugins/fullcalendar/fullcalendar/fullcalendar.css') }}
    {{ HTML::style('plugins/jqvmap/jqvmap/jqvmap.css') }}
    {{ HTML::style('plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css') }}
   <!-- END PAGE LEVEL PLUGIN STYLES -->

   <!-- BEGIN THEME STYLES --> 
   {{ HTML::style('css/style-metronic.css') }}
   {{ HTML::style('css/style.css') }}
   {{ HTML::style('css/style-responsive.css') }}
   {{ HTML::style('css/plugins.css') }}
   {{ HTML::style('css/tasks.css') }}
   {{ HTML::style('css/themes/default.css') }}
   {{ HTML::style('css/custom.css') }}
   <!-- END THEME STYLES -->

  </head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="header-inner">
    <!-- BEGIN LOGO -->
    <a class="navbar-brand" href="{{ URL::to('/') }}">
      Cable TV
    </a>
    <!-- END LOGO -->
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      {{HTML::image('img/menu-toggler.png')}}
    </a>
    <!-- END RESPONSIVE MENU TOGGLER -->
    <!-- BEGIN TOP NAVIGATION MENU -->
    <ul class="nav navbar-nav pull-right">
      
      <!-- BEGIN USER LOGIN DROPDOWN -->
      @if(Auth::check())
      <li class="dropdown user">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
          <!-- <img alt="" src="assets/img/avatar1_small.jpg"/> -->
          <span class="username">
             {{ Auth::user()->email }}
          </span>
          <i class="fa fa-angle-down"></i>
        </a>
       
        <ul class="dropdown-menu">
          @if(Auth::user()->role=='admin')
          <li>
              <i class="fa fa-user"></i> 
              <span>{{ HTML::linkRoute('edit_service_provider', 'My Profile', Auth::user()->admin->id) }}</span>
          </li>
          @endif

          <!-- @if(Auth::user()->role=='service_provider')
          <li>
            <a href="">
              <i class="fa fa-user"></i> Profile
            </a>
          </li>
          @endif -->
          
          <li>
            <a href="{{ URL::to('logout') }}">
              <i class="fa fa-key"></i> Log Out
            </a>
          </li>
        </ul>
      </li>
      @else
        <li><a href="{{ URL::to('login') }}">Login</a></li>
      @endif
      <!-- END USER LOGIN DROPDOWN -->
    </ul>
    <!-- END TOP NAVIGATION MENU -->
  </div>
  <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
      <!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
      <!-- BEGIN SIDEBAR MENU -->
      @if(Auth::check())
      <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
        <li class="sidebar-toggler-wrapper">
          <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
          <div class="sidebar-toggler hidden-phone">
          </div>
          <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li class="sidebar-search-wrapper">
          <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
          <form class="sidebar-search" action="extra_search.html" method="POST">
            <div class="form-container">
              <div class="input-box">
                <a href="javascript:;" class="remove">
                </a>
                <input type="text" placeholder="Search..."/>
                <input type="button" class="submit" value=" "/>
              </div>
            </div>
          </form>
          <!-- END RESPONSIVE QUICK SEARCH FORM -->
        </li>
        <li class="start ">
          <a href="{{ URL::to('/') }}">
            <i class="fa fa-home"></i>
            <span class="title">
              Dashboard
            </span>
            <!-- <span class="selected">
            </span> -->
          </a>
        </li>
        @if(Auth::user()->role=='admin')
        <li>
          <a href="javascript:;">
            <i class="fa fa-shopping-cart"></i>
            <span class="title">
              Admin
            </span>
            <span class="arrow ">
            </span>
          </a>
          <ul class="sub-menu">
            <li>
              <a href="{{ URL::to('admin/index') }}">
                <i class="fa fa-bullhorn"></i>
                Admin List
              </a>
            </li>
            <li>
              <a href="{{ URL::to('admin/create') }}">
                <i class="fa fa-shopping-cart"></i>
                Add New Admin
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="javascript:;">
            <i class="fa fa-shopping-cart"></i>
            <span class="title">
              Service Provider
            </span>
            <span class="arrow ">
            </span>
          </a>
          <ul class="sub-menu">
            <li>
              <a href="{{ URL::to('service_provider/index') }}">
                <i class="fa fa-bullhorn"></i>
                Provider List
              </a>
            </li>
            <li>
              <a href="{{ URL::to('service_provider/create') }}">
                <i class="fa fa-shopping-cart"></i>
                Add New Provider
              </a>
            </li>
          </ul>
        </li>
        @endif

      @if(Auth::user()->role=='service_provider')
        <?php $provider = ServiceProvider::where('user_id', Auth::user()->id)->first() ?>
        <li>
          <a href="{{ URL::to('area/index') }}">
            <i class="fa fa-sitemap"></i>
            <span class="title">
              Area
            </span>
            </span>
          </a>
        </li>

        <li>
          <a href="{{ URL::to('designation/index') }}">
            <i class="fa fa-gift"></i>
            <span class="title">
              Designation
            </span>
          </a>
        </li>
        @if($provider->employee==1)
          <li>
            <a href="javascript:;">
              <i class="fa fa-user"></i>
              <span class="title">
                Employee
              </span>
              <span class="arrow">
              </span>
            </a>
            <ul class="sub-menu">
              <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Employee List">
                <a href="{{ URL::to('employee/index') }}" target="">
                  <span class="title">
                    Employee List
                  </span>
                </a>
              </li>
              <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Add New Employee">
                <a href="{{ URL::to('employee/create') }}" target="">
                  <span class="title">
                    Add New Employee
                  </span>
                </a>
              </li>
            </ul>
          </li>
        @endif
        <li>
          <a href="javascript:;">
            <i class="fa fa-user"></i>
            <span class="title">
              customer
            </span>
            <span class="arrow">
            </span>
          </a>
          <ul class="sub-menu">
            <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Customer List">
              <a href="{{ URL::to('customer/index') }}" target="">
                <span class="title">
                  Customer List
                </span>
              </a>
            </li>
            <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Add New Customer">
              <a href="{{ URL::to('customer/create') }}" target="">
                <span class="title">
                  Add New Customer
                </span>
              </a>
            </li>
          </ul>
        </li>

        @if($provider->operator==1)
        <li>
          <a href="javascript:;">
            <i class="fa fa-user"></i>
            <span class="title">
              Sub-Operator
            </span>
            <span class="arrow">
            </span>
          </a>
          <ul class="sub-menu">
            <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Sub-operator List">
              <a href="{{ URL::to('sub_operator/index') }}" target="">
                <span class="title">
                  Sub-Operator List
                </span>
              </a>
            </li>
            <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Add New Sub-operator">
              <a href="{{ URL::to('sub_operator/create') }}" target="">
                <span class="title">
                  Add New Sub-Operator
                </span>
              </a>
            </li>
          </ul>
        </li>
        @endif

      @if($provider->channel==1)
        <li>
          <a href="javascript:;">
            <i class="fa fa-user"></i>
            <span class="title">
              Pay Channel
            </span>
            <span class="arrow">
            </span>
          </a>
          <ul class="sub-menu">
            <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Pay Channel List">
              <a href="{{ URL::to('pay_channel/index') }}" target="">
                <span class="title">
                  Pay Channel List
                </span>
              </a>
            </li>
            <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Add New Pay Channel">
              <a href="{{ URL::to('pay_channel/create') }}" target="">
                <span class="title">
                  Add New Pay Channel
                </span>
              </a>
            </li>
          </ul>
        </li>
      @endif

        <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Generate Invoice for bill">
          <a href="{{ URL::to('invoice/index') }}">
            <i class="fa fa-gift"></i>
            <span class="title">
              Customer Invoice
            </span>
          </a>
        </li>

      @if($provider->operator==1)
        <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Generate Invoice for bill">
          <a href="{{ URL::to('operator_invoice/index') }}">
            <i class="fa fa-gift"></i>
            <span class="title">
              Sub-Operator Invoice
            </span>
          </a>
        </li>
      @endif

      @if($provider->channel==1)
        <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Generate Invoice for bill">
          <a href="{{ URL::to('channel_invoice/index') }}">
            <i class="fa fa-gift"></i>
            <span class="title">
              Pay Channel Invoice
            </span>
          </a>
        </li>

        
      @endif

    @endif  

        @if(Auth::user()->role=='service_provider')
          <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Generate Invoice for bill">
            <a href="{{ URL::to('bill/index') }}">
              <i class="fa fa-gift"></i>
              <span class="title">
                Customer Bill
              </span>
            </a>
          </li>


          

          @if($provider->operator==1)
            <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Generate Sub-operator Invoice for bill">
              <a href="{{ URL::to('operator_bill/index') }}">
                <i class="fa fa-gift"></i>
                <span class="title">
                  Sub-operator Bill
                </span>
              </a>
            </li>
          @endif

          @if($provider->channel==1)
            <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Generate Invoice for bill">
              <a href="{{ URL::to('channel_bill/index') }}">
                <i class="fa fa-gift"></i>
                <span class="title">
                  Pay Channel Bill
                </span>
              </a>
            </li>
          @endif

        @endif

        @if(Auth::user()->role=='employee')
          <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Generate Invoice for bill">
            <a href="{{ URL::to('bill/index') }}">
              <i class="fa fa-gift"></i>
              <span class="title">
                Customer Bill
              </span>
            </a>
          </li>

          <?php $employee = Employee::where('user_id', Auth::user()->id)->first(); ?>

          

          @if(ServiceProvider::find($employee->provider_id)->operator==1)
            <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Generate Sub-operator Invoice for bill">
              <a href="{{ URL::to('operator_bill/index') }}">
                <i class="fa fa-gift"></i>
                <span class="title">
                  Sub-operator Bill
                </span>
              </a>
            </li>
          @endif

          
         
        @endif

        @if(Auth::user()->role=='service_provider')
          <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Generate Sub-operator Invoice for bill">
            <a href="{{ URL::to('reports/index') }}">
                <i class="fa fa-gift"></i>
              <span class="title">
                  Reports
              </span>
            </a>
          </li>
        @endif
      </ul>
      @endif
      <!-- END SIDEBAR MENU -->
    </div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <div class="page-content">
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
              <button type="button" class="btn blue">Save changes</button>
              <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      
      <!-- END STYLE CUSTOMIZER -->

    @if(Session::has('message'))
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <p class="alert">{{ Session::get('message') }}</p>
      </div>
    @endif

    {{ HTML::ul($errors->all()) }}
    <div class="row">
      @yield('content')
    </div>
      
    </div>
  </div>
  <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
  <div class="footer-inner">
     2014 &copy; Metronic by keenthemes.
  </div>
  <div class="footer-tools">
    <span class="go-top">
      <i class="fa fa-angle-up"></i>
    </span>
  </div>
</div>
<!-- END FOOTER -->

    
 <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE plugins -->   
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script> 
<![endif]--> 
<!-- Scripts are placed here -->
<!-- START CORE PLUGINS -->
{{ HTML::script('plugins/jquery-1.10.2.min.js') }}
{{ HTML::script('plugins/jquery-migrate-1.2.1.min.js') }}
{{ HTML::script('plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}
{{ HTML::script('plugins/bootstrap/js//bootstrap.min.js') }}
{{ HTML::script('plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js') }}
{{ HTML::script('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}
{{ HTML::script('plugins/jquery.cookie.min.js') }}
{{ HTML::script('plugins/uniform/jquery.uniform.min.js') }}
{{ HTML::script('js/bootstrap-datetimepicker.js') }}
{{ HTML::script('js/script.js') }}

{{ HTML::script('plugins/bootstrap-datetimepicker.ru.js') }}

<!-- END CORE PLUGINS -->


<!-- END CORE plugins -->
<!-- BEGIN PAGE LEVEL plugins -->
{{ HTML::script('plugins/jqvmap/jqvmap/jquery.vmap.js') }}
{{ HTML::script('plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}
{{ HTML::script('plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}
{{ HTML::script('plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}
{{ HTML::script('plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}
{{ HTML::script('plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}
{{ HTML::script('plugins/flot/jquery.flot.js') }}
{{ HTML::script('plugins/flot/jquery.flot.resize.js') }}
{{ HTML::script('plugins/jquery.pulsate.min.js') }}
{{ HTML::script('plugins/bootstrap-daterangepicker/moment.min.js') }}
{{ HTML::script('plugins/bootstrap-daterangepicker/daterangepicker.js') }}
{{ HTML::script('plugins/gritter/js/jquery.gritter.js') }}

<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
{{ HTML::script('plugins/fullcalendar/fullcalendar/fullcalendar.min.js') }}
{{ HTML::script('plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}
{{ HTML::script('plugins/jquery.sparkline.min.js') }}

<!-- END PAGE LEVEL plugins -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{{ HTML::script('js/scripts/core/app.js') }}
{{ HTML::script('js/scripts/custom/index.js') }}
{{ HTML::script('js/scripts/custom/tasks.js') }}    
<!-- END PAGE LEVEL SCRIPTS -->  
<script>
  jQuery(document).ready(function() {    
     App.init(); // initlayout and core plugins
     Index.init();
     Index.initJQVMAP(); // init index page's custom scripts
     Index.initCalendar(); // init index page's custom scripts
     Index.initCharts(); // init index page's custom scripts
     Index.initChat();
     Index.initMiniCharts();
     Index.initDashboardDaterange();
     Index.initIntro();
     Tasks.initDashboardWidget();
  });
</script>
<!-- END JAVASCRIPTS -->


  </body>
</html>
