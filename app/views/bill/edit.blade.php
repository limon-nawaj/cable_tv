@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Edit Bill
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="{{ URL::to('bill/index') }}">
                Bill
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="#">
                Edit Bill
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

    <div class="row">
      <div class="col-md-12">
        {{ Form::model($bill, array('method'=>'put', 'route'=>array('update_bill', $bill->id))) }}
            <div class="form-group">
              {{ Form::label('month', 'Month') }}
              {{ Form::select('month', $month, $bill->month, array('class'=>'form-control'), Input::old('month')) }}                
            </div>
            <div class="form-group">
              {{ Form::label('year', 'Year') }}
              {{ Form::select('year', $year, $bill->year, array('class'=>'form-control'), Input::old('year')) }}
            </div>
            <div class="form-group">
              {{ Form::label('vat', 'Vat') }}
              {{ Form::text('vat', $bill->vat, array('class'=>'form-control'), Input::old('vat')) }}
            </div>
            <div class="form-group">
              {{ Form::label('monthly_fee', 'Monthly Fee') }}
              {{ Form::text('monthly_fee', $bill->monthly_fee, array('class'=>'form-control'), Input::old('monthly_fee')) }}
            </div>
            <div class="form-group">
              {{ Form::label('additional_fee', 'Additional Fee') }}
              {{ Form::text('additional_fee', $bill->additional_fee, array('class'=>'form-control'), Input::old('additional_fee')) }}
            </div>
            <div class="col-md-2">
              {{ Form::submit('Update Bill', array('class'=>'btn green')) }}
            </div>
            <div class="form-group">
            {{ Form::label('status', 'Active') }}
            @if($bill->status==1)
              {{ Form::checkbox('status', true) }}Paid
              {{ Form::hidden('status', false) }}
            @else
              {{ Form::checkbox('status', false) }}Not paid
              {{ Form::hidden('status', true) }}
            @endif
          </div>  
          </div>
        {{ Form::close() }}
      </div>
    </div>
@endsection
