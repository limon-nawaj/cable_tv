@extends('commons.default')

@section('content')
	<h1>Admin Registration</h1>
	{{ HTML::ul($errors->all()) }}
	<div class="col-md-8">
		{{ Form::open(array('route'=>'save_admin_path', 'method'=>'POST')) }}
			<div class="form-group">
				{{ Form::label('first_name', 'First Name') }}<br>
				{{ Form::text('first_name', '', array('class'=>'form-control', 'placeholder'=>'First Name'), Input::old('first_name'))}}
			</div>

			<div class="form-group">
				{{ Form::label('last_name', 'Last Name') }}<br>
				{{ Form::text('last_name', '', array('class'=>'form-control', 'placeholder'=>'Last Name'), Input::old('last_name'))}}
			</div>

			{{ Form::hidden('role', 'admin') }}

			<div class="form-group">
				{{ Form::label('email', 'Email') }}<br>
				{{ Form::text('email', '', array('class'=>'form-control', 'placeholder'=>'Email'), Input::old('email'))}}
			</div>

			<div class="form-group">
				{{ Form::label('password', 'Password') }}<br>
				{{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password'), Input::old('password'))}}
			</div>

			<div class="form-group">
				{{ Form::label('mobile', 'Mobile') }}<br>
				{{ Form::text('mobile', '', array('class'=>'form-control', 'placeholder'=>'Mobile'), Input::old('mobile'))}}
			</div>

			<p>{{ Form::submit('Register', array('class'=>'btn green')) }}</p>
		{{ Form::close() }}
	</div>
@endsection