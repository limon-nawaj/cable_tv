@extends('commons.default')

@section('content')
	<!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">
          Dashboard <small>statistics and more</small>
          </h3>
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="index.html">
                Home
              </a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="#">
                Dashboard
              </a>
            </li>
            <li class="pull-right">
              <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="fa fa-calendar"></i>
                <span>
                </span>
                <i class="fa fa-angle-down"></i>
              </div>
            </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->

    <div class="col-md-12">
      <table class="table">
        <tr>
          <th>Name</th>
          <th>Mobile</th>
        </tr>

        @foreach($admins as $admin)
          <tr>
            <td>{{ $admin->first_name }} {{ $admin->last_name }}</td>
            <td>{{ $admin->mobile }}</td>
          </tr>
        @endforeach
      </table>
    </div>
@endsection
