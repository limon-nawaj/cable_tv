<?php

Route::get('/', array('as'=>'home', 'uses'=>'HomeController@home'))->before('auth');
// Route::get('admins', array('as'=>'admin', 'uses'=>'AdminController@index'));

Route::get('admin/index', 			array('as'=>'admins_path', 			'uses'=>'AdminController@index'))->before('auth');
Route::get('admin/create', 			array('as'=>'new_admin_path', 		'uses'=>'AdminController@create'));
Route::post('admin/create', 		array('as'=>'save_admin_path', 		'uses'=>'AdminController@store'));
Route::get('admin/{admin}', 		array('as'=>'admin_path', 			'uses'=>'AdminController@show'))->before('auth');
Route::get('admin/{admin}/edit', 	array('as'=>'edit_admin_path', 		'uses'=>'AdminController@edit'))->before('auth');
Route::put('admin/{admin}', 		array('as'=>'update_admin_path', 	'uses'=>'AdminController@update'))->before('auth');
// Route::patch('admin/{admin}', array('as'=>'admin', 'uses'=>'AdminController@update'));
Route::delete('admin/{admin}', 		array('as'=>'delete_admin_path',	'uses'=>'AdminController@destroy'))->before('auth');


//user controller
Route::get('login', 		array('as' => 'login', 			'uses' => 'UserController@showLogin'));
Route::post('login', 		array('as' => 'user_login',		'uses' => 'UserController@doLogin'));
Route::get('logout', 		array('as' => 'logout', 		'uses' => 'UserController@doLogout'));

Route::get('service_provider/index', 					array('as'=>'service_providers', 			'uses'=>'ServiceProvidersController@index'))->before('auth');
Route::get('service_provider/dashboard',				array('as'=>'provider_dashboard', 			'uses'=>'ServiceProvidersController@dashboard'))->before('auth');
Route::get('service_provider/create', 					array('as'=>'new_service_provider', 		'uses'=>'ServiceProvidersController@create'))->before('auth');
Route::post('service_provider/create', 					array('as'=>'save_service_provider', 		'uses'=>'ServiceProvidersController@store'))->before('auth');
Route::get('service_provider/{service_provider}', 		array('as'=>'service_provider', 			'uses'=>'ServiceProvidersController@show'))->before('auth');
Route::get('service_provider/{id}/edit', 				array('as'=>'edit_service_provider', 		'uses'=>'ServiceProvidersController@edit'))->before('auth');
Route::put('service_provider/{id}/edit',				array('as'=>'update_service_provider', 	'uses'=>'ServiceProvidersController@update'))->before('auth');
// Route::delete('service_provider/{service_provider}', 	array('as'=>'delete_service_provider',	'uses'=>'ServiceProvidersController@destroy'));

Route::get('area/index', 					array('as'=>'areas', 			'uses'=>'AreaController@index'))->before('auth');
Route::post('area/create', 					array('as'=>'save_area', 		'uses'=>'AreaController@store'))->before('auth');
Route::get('area/{id}/edit', 				array('as'=>'edit_area', 		'uses'=>'AreaController@edit'))->before('auth');
Route::put('area/{id}/edit',				array('as'=>'update_area', 		'uses'=>'AreaController@update'	))->before('auth');

Route::get('designation/index', 			array('as'=>'designations', 			'uses'=>'DesignationController@index'))->before('auth');
Route::post('designation/create', 			array('as'=>'save_designation', 		'uses'=>'DesignationController@store'))->before('auth');
Route::get('designation/{id}/edit', 		array('as'=>'edit_designation', 		'uses'=>'DesignationController@edit'))->before('auth');
Route::put('designation/{id}/edit',			array('as'=>'update_designation', 		'uses'=>'DesignationController@update'	))->before('auth');

Route::get('employee/index', 				array('as'=>'employees', 		'uses'=>'EmployeeController@index'))->before('auth');
Route::get('employee/create', 				array('as'=>'new_employee', 	'uses'=>'EmployeeController@create'))->before('auth');
Route::post('employee/create', 				array('as'=>'save_employee', 	'uses'=>'EmployeeController@store'))->before('auth');
Route::get('employee/{employee}', 			array('as'=>'employee', 		'uses'=>'EmployeeController@show'))->before('auth');
Route::get('employee/{id}/edit', 			array('as'=>'edit_employee', 	'uses'=>'EmployeeController@edit'))->before('auth');
Route::put('employee/{id}/edit',			array('as'=>'update_employee', 	'uses'=>'EmployeeController@update'))->before('auth');

Route::get('customer/index', 				array('as'=>'customers', 		'uses'=>'CustomerController@index'))->before('auth');
Route::get('customer/create', 				array('as'=>'new_customer', 	'uses'=>'CustomerController@create'))->before('auth');
Route::post('customer/create', 				array('as'=>'save_customer', 	'uses'=>'CustomerController@store'))->before('auth');
Route::get('customer/{customer}', 			array('as'=>'customer', 		'uses'=>'CustomerController@show'))->before('auth');
Route::get('customer/{id}/edit', 			array('as'=>'edit_customer', 	'uses'=>'CustomerController@edit'))->before('auth');
Route::put('customer/{id}/edit',			array('as'=>'update_customer', 	'uses'=>'CustomerController@update'))->before('auth');
Route::post('customer/{id}',				array('as'=>'save_generate_invoice', 	'uses'=>'CustomerController@generate_invoice'))->before('auth');

Route::get('sub_operator/index', 			array('as'=>'sub_operators', 		'uses'=>'SuboperatorController@index'))->before('auth');
Route::get('sub_operator/create', 			array('as'=>'new_sub_operator', 	'uses'=>'SuboperatorController@create'))->before('auth');
Route::post('sub_operator/create', 			array('as'=>'save_sub_operator', 	'uses'=>'SuboperatorController@store'))->before('auth');
Route::get('sub_operator/{sub_operator}', 	array('as'=>'sub_operator', 		'uses'=>'SuboperatorController@show'))->before('auth');
Route::get('sub_operator/{id}/edit', 		array('as'=>'edit_sub_operator', 	'uses'=>'SuboperatorController@edit'))->before('auth');
Route::put('sub_operator/{id}/edit',		array('as'=>'update_sub_operator', 	'uses'=>'SuboperatorController@update'))->before('auth');
Route::post('sub_operator/{id}',			array('as'=>'operator_invoice', 	'uses'=>'SuboperatorController@operator_invoice'))->before('auth');

Route::get('pay_channel/index', 			array('as'=>'pay_channels', 		'uses'=>'PayChannelController@index'))->before('auth');
Route::get('pay_channel/create', 			array('as'=>'new_pay_channel', 		'uses'=>'PayChannelController@create'))->before('auth');
Route::post('pay_channel/create', 			array('as'=>'save_pay_channel', 	'uses'=>'PayChannelController@store'))->before('auth');
Route::get('pay_channel/{id}', 				array('as'=>'pay_channel', 			'uses'=>'PayChannelController@show'))->before('auth');
Route::get('pay_channel/{id}/edit', 		array('as'=>'edit_pay_channel', 	'uses'=>'PayChannelController@edit'))->before('auth');
Route::put('pay_channel/{id}/edit',			array('as'=>'update_pay_channel', 	'uses'=>'PayChannelController@update'))->before('auth');
Route::get('pay_channel/{id}/payment', 		array('as'=>'new_payment', 			'uses'=>'PayChannelController@payment'))->before('auth');



Route::get('invoice/index', 			array('as'=>'invoices', 		'uses'=>'InvoiceController@index'))->before('auth');
Route::get('invoice/create', 			array('as'=>'new_invoice', 		'uses'=>'InvoiceController@serce_by_area'))->before('auth');
Route::post('invoice/create',			array('as'=>'save_invoice', 	'uses'=>'InvoiceController@store'))->before('auth');
/*Route::get('invoice/{invoice}', 		array('as'=>'invoice', 			'uses'=>'InvoiceController@show'));
Route::get('invoice/{id}/edit', 		array('as'=>'edit_invoice', 	'uses'=>'InvoiceController@edit'));
Route::put('invoice/{id}/edit',			array('as'=>'update_invoice', 	'uses'=>'InvoiceController@update'));
*/

Route::get('operator_invoice/index', 			array('as'=>'operator_invoices', 		'uses'=>'OperatorInvoiceController@index'))->before('auth');
Route::get('operator_invoice/create', 			array('as'=>'new_operator_invoice',		'uses'=>'OperatorInvoiceController@search_by_area'))->before('auth');
Route::post('operator_invoice/create',			array('as'=>'save_operator_invoice', 	'uses'=>'OperatorInvoiceController@store'))->before('auth');

Route::get('channel_invoice/index', 			array('as'=>'channel_invoices', 		'uses'=>'ChannelInvoiceController@index'))->before('auth');
Route::get('channel_invoice/create', 			array('as'=>'new_channel_invoice',		'uses'=>'ChannelInvoiceController@create'))->before('auth');
Route::post('channel_invoice/create',			array('as'=>'save_channel_invoice', 	'uses'=>'ChannelInvoiceController@store'))->before('auth');


Route::get('bill/index', 			array('as'=>'bills', 		'uses'=>'BillController@index'))->before('auth');
Route::get('bill/search',			array('as'=>'search', 		'uses'=>'BillController@search_by_area'))->before('auth');
Route::get('bill/create', 			array('as'=>'new_bill', 	'uses'=>'BillController@create'))->before('auth');
Route::post('bill/create',			array('as'=>'save_bill', 	'uses'=>'BillController@store'))->before('auth');
Route::get('bill/{bill}', 			array('as'=>'bill', 		'uses'=>'BillController@show'))->before('auth');
Route::get('bill/{id}/edit', 		array('as'=>'edit_bill', 	'uses'=>'BillController@edit'))->before('auth');
Route::put('bill/{id}/edit',		array('as'=>'update_bill', 	'uses'=>'BillController@update'))->before('auth');
// Route::get('bill/search/{id}/edit', array('as'=>'s_edit_bill', 	'uses'=>'BillController@s_edit'));
// Route::put('bill/search/{id}/edit',	array('as'=>'s_update_bill','uses'=>'BillController@s_update'));
Route::put('bill/index/',			array('as'=>'paid_bill',	'uses'=>'BillController@paid'))->before('auth');
Route::put('bill/search',			array('as'=>'s_paid_bill', 		'uses'=>'BillController@s_paid'))->before('auth');
// Route::put('bill/index',			array('as'=>'unpaid_bill',	'uses'=>'BillController@unpaid'));
// http://localhost:8000/bill/search?area=1
Route::get('operator_bill/index', 			array('as'=>'operator_bills', 		'uses'=>'OperatorBillController@index'))->before('auth');
Route::get('operator_bill/search',			array('as'=>'operator_search', 		'uses'=>'OperatorBillController@search_by_area'))->before('auth');
Route::get('operator_bill/create', 			array('as'=>'new_operator_bill', 	'uses'=>'OperatorBillController@create'))->before('auth');
Route::post('operator_bill/create',			array('as'=>'save_operator_bill', 	'uses'=>'OperatorBillController@store'))->before('auth');
Route::get('operator_bill/{operator_bill}', array('as'=>'operator_bill', 		'uses'=>'OperatorBillController@show'))->before('auth');
Route::get('operator_bill/{id}/edit', 		array('as'=>'edit_operator_bill', 	'uses'=>'OperatorBillController@edit'))->before('auth');
Route::put('operator_bill/{id}/edit',		array('as'=>'update_operator_bill', 'uses'=>'OperatorBillController@update'))->before('auth');
Route::put('operator_bill/index/',			array('as'=>'paid_operator_bill',	'uses'=>'OperatorBillController@paid'))->before('auth');
// Route::put('operator_bill/index',			array('as'=>'s_paid_operator_bill', 'uses'=>'OperatorBillController@s_paid'))->before('auth');

Route::get('channel_bill/index', 			array('as'=>'channel_bills', 		'uses'=>'ChannelBillController@index'))->before('auth');
Route::get('channel_bill/search',			array('as'=>'channel_search', 		'uses'=>'ChannelBillController@search'))->before('auth');
Route::get('channel_bill/create', 			array('as'=>'new_channel_bill', 	'uses'=>'ChannelBillController@create'))->before('auth');
Route::post('channel_bill/create',			array('as'=>'save_channel_bill', 	'uses'=>'ChannelBillController@store'))->before('auth');
Route::get('channel_bill/{channel_bill}', 	array('as'=>'channel_bill', 		'uses'=>'ChannelBillController@show'))->before('auth');
Route::get('channel_bill/{id}/edit', 		array('as'=>'edit_channel_bill', 	'uses'=>'ChannelBillController@edit'))->before('auth');
Route::put('channel_bill/{id}/edit',		array('as'=>'update_channel_bill', 'uses'=>'ChannelBillController@update'))->before('auth');
Route::put('channel_bill/index/',			array('as'=>'paid_channel_bill',	'uses'=>'ChannelBillController@paid'))->before('auth');
// Route::put('channel_bill/index',			array('as'=>'s_paid_channel_bill', 'uses'=>'ChannelBillController@s_paid'))->before('auth');


//Reports
Route::get('reports/index', array('as'=>'reports_index', 'uses'=>'ReportController@index'))->before('auth');
Route::get('reports/monthly_due', array('as'=>'current_month_due', 'uses'=>'ReportController@find_current_month_year_due'))->before('auth');
Route::get('reports/all_due', array('as'=>'all_due', 'uses'=>'ReportController@all_due'))->before('auth');
Route::get('reports/all_paid', array('as'=>'all_paid', 'uses'=>'ReportController@all_paid'))->before('auth');
Route::get('reports/individual_due', array('as'=>'current_month_individual_due', 'uses'=>'ReportController@current_month_individual_due'))->before('auth');
Route::get('reports/individual_paid_due', array('as'=>'individual_paid_due', 'uses'=>'ReportController@individual_paid_and_due'))->before('auth');
Route::get('reports/date_range_due', array('as'=>'date_range_due', 'uses'=>'ReportController@date_range_due'))->before('auth');
Route::get('reports/date_range_paid', array('as'=>'date_range_paid', 'uses'=>'ReportController@date_range_paid'))->before('auth');
//Reports