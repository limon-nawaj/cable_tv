<?php

class AdminController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$admins = Admin::all();
		return View::make('admin.index')->with('admins', $admins);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.new');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'first_name' 	=> 'required',
			'last_name' 	=> 'required',
			'email' 		=> 'required|email|unique:users',
			'password'		=> 'required|between: 4,20',
			'mobile'		=> 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('admin/create')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			$user 				= new User;
			$user->email 		= Input::get('email');
			$user->password 	= Hash::make(Input::get('password'));
			$user->role 		= Input::get('role');

			

			$admin 				= new Admin;
			// $admn->user_id 		= $user->id;
			$admin->first_name	= Input::get('first_name');
			$admin->last_name	= Input::get('last_name');
			$admin->mobile		= Input::get('mobile');

			$user->save();
			$admin->user_id 		= $user->id;
			$admin->save();

			Session::flash('message', 'Successfully created');
			return Redirect::to('/');
		}

		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
