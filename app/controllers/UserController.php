<?php 

/**
* 
*/
class UserController extends BaseController
{
	public function showLogin()
	{
		$admin = Admin::get();
		return View::make('user.login')->with('admin', $admin);
	}

	public function doLogin(){
		$user = array('email'=>Input::get('email'), 'password'=>Input::get('password'));

		if (Auth::attempt($user)) {
			/*if (Auth::user()->role=='admin') {
				return Redirect::to('admin/index'); 
			} elseif(Auth::user()->role=='service_provider') {
				return Redirect::route('home');
			} elseif(Auth::user()->role=='employee'){
				
			}*/
			return Redirect::route('home');
		}else{
			return Redirect::route('user_login')->with('message', 'your email/password was invalid')->withInput();
		}
	}

	public function doLogout(){
		Auth::logout();
		return Redirect::to('login')->with('message', 'your successfully logout');
	}
}