<?php

class EmployeeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$employees = Employee::where('provider_id', $provider->id)->get();
		return View::make('employee.index')->with('employees', $employees);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$designation = array('' => 'Select Designations') + Designation::select(DB::raw('concat (title) as title, id'))->where('provider_id', $provider->id)->lists('title', 'id');
		return View::make('employee.new')->with('designation', $designation);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$rules = array(
			'first_name' 	=> 'required',
			'last_name' 	=> 'required',
			'mobile' 		=> 'required',
			'designation_id'=> 'required',
			'email'			=> 'required|email|unique:users',
			'password'		=> 'required|between:4,20',
			'join_date' 	=> 'required',
			'basic_salary' 	=> 'required|numeric|digits_between:2,10',
			'other_salary'  => 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('employee/create')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			$user 				= new User;
			$user->email 		= Input::get('email');
			$user->password 	= Hash::make(Input::get('password'));
			$user->role 		= 'employee';

			$employee 			= new Employee;

			$employee->provider_id			= $provider->id;
			$employee->first_name			= Input::get('first_name');
			$employee->last_name			= Input::get('last_name');
			$employee->mobile				= Input::get('mobile');
			$employee->join_date			= Input::get('join_date');
			$employee->basic_salary			= Input::get('basic_salary');
			$employee->other_salary			= Input::get('other_salary');
			$employee->designation_id		= Input::get('designation_id');
			$employee->present_address		= Input::get('present_address');
			$employee->permanent_address	= Input::get('permanent_address');
			$employee->note					= Input::get('note');
			$employee->status 				= 1;

			$user->save();
			$employee->user_id				= $user->id;
			$employee->save();

			Session::flash('message', 'Successfully created');
			return Redirect::to('employee/index');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$employee = Employee::find($id);
		$designation = array('' => 'Select Designations') + Designation::select(DB::raw('concat (title) as title, id'))->where('provider_id', $provider->id)->lists('title', 'id');
		return View::make('employee.edit')->with('employee', $employee)->with('designation', $designation);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$rules = array(
			'first_name' 	=> 'required',
			'last_name' 	=> 'required',
			'mobile' 		=> 'required',
			'password'		=> 'between:4,20',
			'join_date' 	=> 'required',
			'basic_salary' 	=> 'required|numeric|digits_between:2,10',
			'other_salary'	=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('employee/'.$id.'/edit')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			$employee 						= Employee::find($id);

			$user 							= User::find($employee->user_id);
			if (!empty($data['password'])) {
				$user->password 			= Hash::make(Input::get('password'));
			}
			$user->role 					= 'employee';

			$employee->provider_id			= $provider->id;
			$employee->first_name			= Input::get('first_name');
			$employee->last_name			= Input::get('last_name');
			$employee->mobile				= Input::get('mobile');
			$employee->join_date			= Input::get('join_date');
			$employee->basic_salary			= Input::get('basic_salary');
			$employee->other_salary			= Input::get('other_salary');
			$employee->designation_id		= Input::get('designation_id');
			$employee->present_address		= Input::get('present_address');
			$employee->permanent_address	= Input::get('permanent_address');
			$employee->note					= Input::get('note');

			if (Input::get('status')==true) {
				$employee->status 	= 1;
			} else {
				$employee->status 	= 0;
			}

			$user->save();
			$employee->user_id				= $user->id;
			$employee->save();

			Session::flash('message', 'Successfully created');
			return Redirect::to('employee/index');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
