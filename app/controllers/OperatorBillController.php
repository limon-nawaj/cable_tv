<?php

class OperatorBillController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::user()->role=='service_provider') {
			$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		} elseif(Auth::user()->role=='employee') {
			$employee = Employee::where('user_id', Auth::user()->id)->first();
			$provider = ServiceProvider::find($employee->provider_id);
		}

		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');

		$rules = array('month'=>'required', 'year'=>'required');

		$validator = Validator::make(Input::all(), $rules);

		
		if (Input::get('area')!=null) {
			$operator_invoices = OperatorInvoice::orderBy('id', 'desc')->groupBy('operator_id')
			->where('area_id', Input::get('area'))->where('provider_id', $provider->id)->get();
			return View::make('operator_bill.index')->with('month', $month)->with('year', $year)->with('operator_invoices', $operator_invoices)->with('area', $area);	
		}else{
			$operator_invoices = OperatorInvoice::orderBy('id', 'desc')->groupBy('operator_id')
			->where('provider_id', $provider->id)->get();	
			return View::make('operator_bill.index')->with('month', $month)->with('year', $year)->with('operator_invoices', $operator_invoices)->with('area', $area);	
		}
	}

	public function search_by_area()
	{
		if (Auth::user()->role=='service_provider') {
			$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		} elseif(Auth::user()->role=='employee') {
			$employee = Employee::where('user_id', Auth::user()->id)->first();
			$provider = ServiceProvider::find($employee->provider_id);
		}
		
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');


		

		$area_id = Input::has('area');
		if ($area_id!='') {
			$operator_invoices = OperatorInvoice::orderBy('id', 'desc')->groupBy('operator_id')
			->where('area_id', Input::get('area'))->where('provider_id', $provider->id)->get();
		} else {
			$operator_invoices = OperatorInvoice::orderBy('id', 'desc')->groupBy('operator_id')
								->where('provider_id', $provider->id)->get();
		}
		

		return View::make('operator_bill.search')->with('month', $month)->with('year', $year)
					->with('operator_invoices', $operator_invoices)->with('area', $area)->with('area_id', Input::get('area'));	
		/*$rules = array('month'=>'required', 'year'=>'required');

		$validator = Validator::make(Input::all(), $rules);

		
		if (Input::has('area')) {
			$operator_invoices = OperatorInvoice::orderBy('id', 'desc')->groupBy('operator_id')
			->where('area_id', Input::get('area'))->where('provider_id', $provider->id)->get();
			return View::make('operator_bill.search')->with('month', $month)->with('year', $year)
					->with('operator_invoices', $operator_invoices)->with('area', $area)->with('area_id', Input::get('area'));	
		}*/
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$operator_bill = OperatorInvoice::find($id);
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		return View::make('operator_bill.edit')->with('operator_bill', $operator_bill)->with('month', $month)->with('year', $year);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'vat'			=> 'numeric',
			'monthly_fee'	=> 'required|numeric',
			'month'			=> 'required',
			'year'			=> 'required',
			'additional_fee'=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('operator_bill/'.$id.'/edit')->withErrors($validator);
		} else {
			$operator_bill = OperatorInvoice::find($id);

			$vat = Input::get('vat');
			$monthly_fee = Input::get('monthly_fee');
			$additional_fee = Input::get('additional_fee');

			$operator_bill->month 			= Input::get('month');
			$operator_bill->year 			= Input::get('year');
			$operator_bill->vat 				= $vat;
			$operator_bill->monthly_fee 		= $monthly_fee;
			$operator_bill->additional_fee 	= $additional_fee;
			$operator_bill->total_fee 		= $monthly_fee + ($monthly_fee*$vat)/100+$additional_fee;

			/*if (Input::get('status')==true) {
				$operator_bill->status 		= 1;
			} else {
				$operator_bill->status 		= 0;
			}*/

			$operator_bill->save();
			Session::flash('message', 'Successfully updated');
			return Redirect::to('operator_bill/index');
		}
		
		
	}

	public function s_edit($id)
	{
		$operator_bill = OperatorInvoice::find($id);
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		return View::make('operator_bill.s_edit')->with('operator_bill', $operator_bill)->with('month', $month)->with('year', $year);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function s_update($id)
	{
		$operator_bill = OperatorInvoice::find($id);

		$rules = array(
			'vat'			=> 'numeric',
			'monthly_fee'	=> 'required|numeric',
			'month'			=> 'required',
			'year'			=> 'required',
			'additional_fee'=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('operator_bill/search/'.$id.'/edit')->withErrors($validator);
		} else {
			

			$vat = Input::get('vat');
			$monthly_fee = Input::get('monthly_fee');
			$additional_fee = Input::get('additional_fee');

			$operator_bill->month 			= Input::get('month');
			$operator_bill->year 			= Input::get('year');
			$operator_bill->vat 			= $vat;
			$operator_bill->monthly_fee 	= $monthly_fee;
			$operator_bill->additional_fee 	= $additional_fee;
			$operator_bill->total_fee 		= $monthly_fee + ($monthly_fee*$vat)/100+$additional_fee;

			$operator_bill->save();
			Session::flash('message', 'Successfully updated');
			return Redirect::to('operator_bill/search?area='.$operator_bill->area_id);
		}
	}

	public function paid()
	{
		$rules = array('credit'=>'required|numeric');
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {
			$operator_bill = new OperatorBill;

			$get_debit = OperatorBill::where('operator_id', Input::get('operator_id'))->where('credit', 0)->orderBy('id', 'desc')->first();
			$get_credit = OperatorBill::where('operator_id', Input::get('operator_id'))->where('debit', 0)->orderBy('id', 'desc')->first();

			$operator_bill->invoice_id = Input::get('invoice_id');
			$operator_bill->operator_id = Input::get('operator_id');
			$operator_bill->credit = Input::get('credit');

			if (!empty($get_credit)) {
				$operator_bill->total = OperatorBill::where('operator_id', Input::get('operator_id'))->where('invoice_id', Input::get('invoice_id'))->where('debit', 0)->orderBy('id', 'desc')->sum('credit')+Input::get('credit')-$get_debit->debit;
			} else {
				$operator_bill->total = Input::get('credit')-$get_debit->debit;
			}
			$operator_bill->save();

			if (!empty(Input::get('area_id'))) {
				return Redirect::to('operator_bill/search?area='.Input::get('area_id'));
			} else if (Input::get('page')=='individual'){
				return Redirect::to('sub_operator/'.$operator_bill->operator_id);
			} else {
				return Redirect::to('operator_bill/index');
			}
		}
		
	}

	public function s_paid()
	{
		$rules = array('credit'=>'required|numeric');
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {
			$operator_bill = new OperatorBill;

			$get_debit = OperatorBill::where('operator_id', Input::get('operator_id'))->where('credit', 0)->orderBy('id', 'desc')->first();
			$get_credit = OperatorBill::where('operator_id', Input::get('operator_id'))->where('debit', 0)->orderBy('id', 'desc')->first();

			$operator_bill->invoice_id = Input::get('invoice_id');
			$operator_bill->operator_id = Input::get('operator_id');
			$operator_bill->credit = Input::get('credit');

			if (!empty($get_credit)) {
				$operator_bill->total = OperatorBill::where('operator_id', Input::get('operator_id'))->where('invoice_id', Input::get('invoice_id'))->where('debit', 0)->orderBy('id', 'desc')->sum('credit')+Input::get('credit')-$get_debit->debit;
			} else {
				$operator_bill->total = Input::get('credit')-$get_debit->debit;
			}
			$operator_bill->save();
			return Redirect::to('operator_bill/search?area='.Input::get('area_id'));

		}
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
