<?php

class PayChannelController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$pay_channels = PayChannel::where('provider_id', $provider->id)->get();
		
		return View::make('pay_channel.index')->with('pay_channels', $pay_channels);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->where('status', 1)->lists('name', 'id');
		return View::make('pay_channel.new')->with('area', $area);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$rules = array(
			'chan_id'			=> 'required|between:4, 12|unique:paychannels',
			'first_name' 		=> 'required',
			'last_name' 		=> 'required',
			'flat'				=> 'required',
			'house'				=> 'required',
			'road'				=> 'required',
			'area'				=> 'required',
			'mobile'			=> 'required',
			'connection_date'	=> 'required',
			'connection_fee' 	=> 'numeric',
			'monthly_fee'		=> 'numeric|required',
			'monthly_vat'		=> 'numeric',
			'discount'			=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('pay_channel/create')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			$pay_channel = new PayChannel;

			$pay_channel->chan_id			= Input::get('chan_id');
			$pay_channel->provider_id 		= $provider->id;
			$pay_channel->first_name 		= Input::get('first_name');
			$pay_channel->last_name 		= Input::get('last_name');
			$pay_channel->flat 				= Input::get('flat');
			$pay_channel->house 			= Input::get('house');
			$pay_channel->road 				= Input::get('road');
			$pay_channel->block 			= Input::get('block');
			$pay_channel->area 				= Input::get('area');
			$pay_channel->phone 			= Input::get('phone');
			$pay_channel->mobile 			= Input::get('mobile');
			$pay_channel->connection_date 	= Input::get('connection_date');
			$pay_channel->connection_fee 	= Input::get('connection_fee');
			$pay_channel->monthly_fee 		= Input::get('monthly_fee');
			$pay_channel->monthly_vat 		= Input::get('monthly_vat');
			// $pay_channel->discount 		= Input::get('discount');
			$pay_channel->status 			= 1;
			$pay_channel->note 				= Input::get('note');

			$pay_channel->save();

			Session::flash('message', 'Successfully created');
			return Redirect::to('pay_channel/index');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$channel = PayChannel::find($id);
		$invoices 	= ChannelInvoice::where('channel_id', $channel->id)->get();
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);
		return View::make('pay_channel.show')->with('invoices', $invoices)
				->with('channel', $channel)->with('month', $month)->with('year', $year);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$pay_channel = PayChannel::find($id);
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');
		return View::make('pay_channel.edit')->with('area', $area)->with('pay_channel', $pay_channel);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$rules = array(
			'chan_id'			=> 'required|between:4, 12|unique:paychannels,chan_id,'.$id,
			'first_name' 		=> 'required',
			'last_name' 		=> 'required',
			'house'				=> 'required',
			'road'				=> 'required',
			'area'				=> 'required',
			'mobile'			=> 'required',
			'connection_date'	=> 'required',
			'connection_fee' 	=> 'numeric',
			'monthly_fee'		=> 'numeric|required',
			'monthly_vat'		=> 'numeric',
			'additional_fee'	=> 'numeric',
			'discount'			=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('pay_channel/'.$id.'/edit')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			$pay_channel = PayChannel::find($id);

			$pay_channel->chan_id				= Input::get('chan_id');
			$pay_channel->provider_id 			= $provider->id;
			$pay_channel->first_name 			= Input::get('first_name');
			$pay_channel->last_name 			= Input::get('last_name');
			$pay_channel->flat 				= Input::get('flat');
			$pay_channel->house 				= Input::get('house');
			$pay_channel->road 				= Input::get('road');
			$pay_channel->block 				= Input::get('block');
			$pay_channel->area 				= Input::get('area');
			$pay_channel->phone 				= Input::get('phone');
			$pay_channel->mobile 				= Input::get('mobile');
			$pay_channel->connection_date 		= Input::get('connection_date');
			$pay_channel->connection_fee 		= Input::get('connection_fee');
			$pay_channel->monthly_fee 			= Input::get('monthly_fee');
			$pay_channel->monthly_vat 			= Input::get('monthly_vat');
			$pay_channel->discount 			= Input::get('discount');
			$pay_channel->status 				= 1;
			$pay_channel->note 				= Input::get('note');

			if (Input::get('status')==true) {
				$pay_channel->status 		= 1;
			} else {
				$pay_channel->status 		= 0;
			}

			$pay_channel->save();

			Session::flash('message', 'Successfully created');
			return Redirect::to('pay_channel/index');
		}
	}

	public function payment($id)
	{
		$pay_channel = PayChannel::find($id);
		$type = array('monthly'=>'Monthly', 'other'=>'Other');
		$channel_id = Input::get('channel_id');
		return View::make('pay_channel/payment')->with('type', $type)->with('pay_channel', $pay_channel);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
