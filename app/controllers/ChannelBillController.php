<?php

class ChannelBillController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');

		$rules = array('month'=>'required', 'year'=>'required');

		$validator = Validator::make(Input::all(), $rules);

		
		if (Input::get('area')!=null) {
			$channel_invoices = ChannelInvoice::orderBy('id', 'desc')->groupBy('channel_id')
			->where('area_id', Input::get('area'))->where('provider_id', $provider->id)->get();
			return View::make('channel_bill.index')->with('month', $month)->with('year', $year)->with('channel_invoices', $channel_invoices)->with('area', $area);	
		}else{
			$channel_invoices = ChannelInvoice::orderBy('id', 'desc')->groupBy('channel_id')
			->where('provider_id', $provider->id)->get();	
			return View::make('channel_bill.index')->with('month', $month)->with('year', $year)->with('channel_invoices', $channel_invoices)->with('area', $area);	
		}
	}

	public function search()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');

		$rules = array('month'=>'required', 'year'=>'required');

		$validator = Validator::make(Input::all(), $rules);

		
		if (Input::has('area')) {
			$channel_invoices = ChannelInvoice::orderBy('id', 'desc')->groupBy('channel_id')
			->where('area_id', Input::get('area'))->where('provider_id', $provider->id)->get();
			return View::make('channel_bill.search')->with('month', $month)->with('year', $year)
					->with('channel_invoices', $channel_invoices)->with('area', $area)->with('area_id', Input::get('area'));	
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$rules 	= array(
			'credit' => 'required',
			'type' => 'required',
			'payment_date' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return View::make('pay_channel/index')->withErrors($validator);
		} else {
			$channel_bill = new ChannelBill;

			$channel_bill->channel_id 	= Input::get('channel_id');
			$channel_bill->credit    	= Input::get('credit');
			$channel_bill->type 		= Input::get('type');
			$channel_bill->payment_date = Input::get('payment_date');

			$channel_bill->save();
			return Redirect::to('pay_channel/index');
		}
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
	}

	public function destroy($id)
	{
		//
	}

	public function paid()
	{
		$rules = array('credit'=>'required|numeric');
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {
			$bill = new ChannelBill;

			$get_debit = ChannelBill::where('channel_id', Input::get('channel_id'))->where('credit', 0)->orderBy('id', 'desc')->first();
			$get_credit = ChannelBill::where('channel_id', Input::get('channel_id'))->where('debit', 0)->orderBy('id', 'desc')->first();

			$bill->invoice_id = Input::get('invoice_id');
			$bill->channel_id = Input::get('channel_id');
			$bill->credit = Input::get('credit');
			$bill->created_at 		= date("Y-m-d");

			if (!empty($get_credit)) {
				$bill->total = ChannelBill::where('channel_id', Input::get('channel_id'))->where('invoice_id', Input::get('invoice_id'))->where('debit', 0)->orderBy('id', 'desc')->sum('credit')+Input::get('credit')-$get_debit->debit;
			} else {
				$bill->total = Input::get('credit')-$get_debit->debit;
			}
			$bill->save();
			return Redirect::to('channel_bill/index');
		}
		
	}

}
