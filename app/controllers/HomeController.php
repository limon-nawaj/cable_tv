<?php

class HomeController extends BaseController {

	public function home()
	{
		if (Auth::user()->role=='admin') {
			return View::make('home.admin_dashboard'); 
		} elseif(Auth::user()->role=='service_provider') {
			$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
			$customers = Customer::where('provider_id', $provider->id)->orderBy('id', 'desc')->take(10)->get();
			$sub_operators = SubOperator::where('provider_id', $provider->id)->orderBy('id', 'desc')->take(10)->get();
			$pay_channels = PayChannel::where('provider_id', $provider->id)->orderBy('id', 'desc')->take(10)->get();
			$employees = Employee::where('provider_id', $provider->id)->orderBy('id', 'desc')->take(10)->get();
			if ($provider->status == 1) {
				return View::make('home.provider_dashboard')
				->with('provider', $provider)->with('customers', $customers)
				->with('sub_operators', $sub_operators)
				->with('pay_channels', $pay_channels)
				->with('employees', $employees);
			} else {
				Auth::logout();
				return Redirect::to('login')->with('message', 'Your service is not available');
			}
			
		}elseif(Auth::user()->role=='employee'){
			$employee = Employee::where('user_id', Auth::user()->id)->first();
			$provider = ServiceProvider::find($employee->provider_id);
			if ($provider->status==1) {
				return View::make('home.employee_dashboard')
					->with('employee', $employee)
					->with('provider', $provider);
			} else {
				Auth::logout();
				return Redirect::to('login')->with('message', 'Your service is not available');
			}
			
			
		}
	}

}
