<?php

class DesignationController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$designations = Designation::where('provider_id', $provider->id)->get();
		return View::make('designation.index')->with('designations', $designations);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'title' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('designation/index')->withErrors($validator);
		} else {
			$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
			$designation = new Designation;

			$designation->title = Input::get('title');
			$designation->status = 1;
			$designation->provider_id = $provider->id;
			$designation->save();
			Session::flash('message', 'Designation added successfully');
			return Redirect::to('designation/index');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$designation = Designation::find($id);
		return View::make('designation.edit')->with('designation', $designation);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'title' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('designation/'.$id.'/edit')->withErrors($validator);
		} else {
			$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
			
			$designation = Designation::find($id);

			$designation->title = Input::get('title');
			if (Input::get('status')==true) {
				$designation->status 		= 1;
			} else {
				$designation->status 		= 0;
			}
			$designation->provider_id = $provider->id;
			$designation->save();
			Session::flash('message', 'Designation updated successfully');
			return Redirect::to('designation/index');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
