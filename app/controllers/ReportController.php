<?php

class ReportController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$user_type = array('Customer'=>'Customer', 'Sub-Operator'=>'Sub-Operator', 'PayChannel'=>'Pay Channel');
		
		return View::make('reports.index')->with('user_type', $user_type);
	}

	public function find_current_month_year_due()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		if (Input::get('user_type')=='Customer') {
			$customers = Customer::where('provider_id', $provider->id)->get();
			return View::make('reports.current_month_due')->with('customers', $customers);
		}else if (Input::get('user_type')=='Sub-Operator') {
			$operators = SubOperator::where('provider_id', $provider->id)->get();
			return View::make('reports.current_month_due')->with('operators', $operators);
		}else if (Input::get('user_type')=='PayChannel') {
			$channels = PayChannel::where('provider_id', $provider->id)->get();
			return View::make('reports.current_month_due')->with('channels', $channels);
		}
	}

	public function all_due()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		if (Input::get('user_type')=='Customer') {
			$customers = Customer::where('provider_id', $provider->id)->get();
			return View::make('reports.all_due')->with('customers', $customers);
		}else if (Input::get('user_type')=='Sub-Operator') {
			$operators = SubOperator::where('provider_id', $provider->id)->get();
			return View::make('reports.all_due')->with('operators', $operators);
		}else if (Input::get('user_type')=='PayChannel') {
			$channels = PayChannel::where('provider_id', $provider->id)->get();
			return View::make('reports.all_due')->with('channels', $channels);
		}	
	}

	public function current_month_individual_due()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$id = Input::get("id");

		if (Input::get('user_type')=='Customer') {
			$customer = Customer::where('provider_id', $provider->id)->where('cust_id', $id)->first();
			return View::make('reports.current_month_individual_due')->with('customer', $customer);
		}else if (Input::get('user_type')=='Sub-Operator') {
			$operator = SubOperator::where('provider_id', $provider->id)->where('opt_id', $id)->first();
			return View::make('reports.current_month_individual_due')->with('operator', $operator);
		}else if (Input::get('user_type')=='PayChannel') {
			$channel = PayChannel::where('provider_id', $provider->id)->where('chan_id', $id)->first();;
			return View::make('reports.current_month_individual_due')->with('channel', $channel);
		}
	}

	public function individual_paid_and_due()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$id = Input::get("id");

		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		if (Input::get('user_type')=='Customer') {
			$customer = Customer::where('provider_id', $provider->id)->where('cust_id', $id)->first();
			$invoices 	= Invoice::where('customer_id', $customer->id)->get();
			return View::make('reports.individual_paid_due')->with('customer', $customer)
					->with('invoices', $invoices)->with('month', $month)->with('year', $year);
		}else if (Input::get('user_type')=='Sub-Operator') {
			$operator = SubOperator::where('provider_id', $provider->id)->where('opt_id', $id)->first();
			$invoices 	= OperatorInvoice::where('operator_id', $operator->id)->get();
			return View::make('reports.individual_paid_due')->with('operator', $operator)
					->with('invoices', $invoices)->with('month', $month)->with('year', $year);
		}else if (Input::get('user_type')=='PayChannel') {
			$channel = PayChannel::where('provider_id', $provider->id)->where('chan_id', $id)->first();
			$invoices 	= ChannelInvoice::where('channel_id', $channel->id)->get();
			return View::make('reports.individual_paid_due')->with('channel', $channel)
					->with('invoices', $invoices)->with('month', $month)->with('year', $year);
		}	
	}


	public function all_paid()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		if (Input::get('user_type')=='Customer') {
			$customers = Customer::where('provider_id', $provider->id)->get();
			return View::make('reports.all_paid')->with('customers', $customers);
		}else if (Input::get('user_type')=='Sub-Operator') {
			$operators = SubOperator::where('provider_id', $provider->id)->get();
			return View::make('reports.all_paid')->with('operators', $operators);
		}else if (Input::get('user_type')=='PayChannel') {
			$channels = PayChannel::where('provider_id', $provider->id)->get();
			return View::make('reports.all_paid')->with('channels', $channels);
		}	
	}

	public function date_range_due()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$startDate = Input::get('start_date');
		$endDate = Input::get('end_date');

		if (Input::get('user_type')=='Customer') {
			$customers = Customer::where('provider_id', $provider->id)->get();
			return View::make('reports.date_range_due')->with('customers', $customers)->with('startDate', $startDate)->with('endDate', $endDate);
		}else if (Input::get('user_type')=='Sub-Operator') {
			$operators = SubOperator::where('provider_id', $provider->id)->get();
			return View::make('reports.date_range_due')->with('operators', $operators)->with('startDate', $startDate)->with('endDate', $endDate);;
		}else if (Input::get('user_type')=='PayChannel') {
			$channels = PayChannel::where('provider_id', $provider->id)->get();
			return View::make('reports.date_range_due')->with('channels', $channels)->with('startDate', $startDate)->with('endDate', $endDate);;
		}	
	}

	public function date_range_paid()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$startDate = Input::get('start_date');
		$endDate = Input::get('end_date');

		if (Input::get('user_type')=='Customer') {
			$customers = Customer::where('provider_id', $provider->id)->get();
			return View::make('reports.date_range_due')->with('customers', $customers)->with('startDate', $startDate)->with('endDate', $endDate);
		}else if (Input::get('user_type')=='Sub-Operator') {
			$operators = SubOperator::where('provider_id', $provider->id)->get();
			return View::make('reports.date_range_due')->with('operators', $operators)->with('startDate', $startDate)->with('endDate', $endDate);;
		}else if (Input::get('user_type')=='PayChannel') {
			$channels = PayChannel::where('provider_id', $provider->id)->get();
			return View::make('reports.date_range_due')->with('channels', $channels)->with('startDate', $startDate)->with('endDate', $endDate);;
		}	
	}


}
