<?php

class ServiceProvidersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$service_providers = ServiceProvider::all();
		return View::make('service_provider.index')->with('service_providers', $service_providers);
	}

	/*public function dashboard()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		if (Auth::user()->role=="service_provider") {
			$customers = Customer::where('provider_id', $provider->id)->orderBy('id', 'desc')->take(10)->get();
			$sub_operators = SubOperator::where('provider_id', $provider->id)->orderBy('id', 'desc')->take(10)->get();
			return View::make('service_provider.dashboard')
				->with('provider', $provider)->with('customers', $customers)
				->with('sub_operators', $sub_operators);
		}else{
			return Redirect::route('user_login')->with('message', 'Invalid access');
		}
		
	}*/


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('service_provider.new');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'first_name' 	=> 'required',
			'last_name' 	=> 'required',
			'email' 		=> 'required|email|unique:users',
			'password'		=> 'required|between: 4,20',
			'mobile'		=> 'required',
			'join_date'		=> 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('service_provider/create')->withErrors($validator)->withInput(Input::except('password'));
		} else{
			$user 							= new User;
			$user->email 					= Input::get('email');
			$user->password 				= Hash::make(Input::get('password'));
			$user->role 					= Input::get('role');

			$service_provider   			= new ServiceProvider;
			$service_provider->first_name	= Input::get('first_name');
			$service_provider->last_name	= Input::get('last_name');
			$service_provider->company		= Input::get('company');
			$service_provider->join_date	= Input::get('join_date');
			$service_provider->mobile		= Input::get('mobile');
			$service_provider->status		= Input::get('status');

			if (Input::get('employee')==true) {
				$service_provider ->employee	= 1;
			} else {
				$service_provider->employee		= 0;
			}

			if (Input::get('operator')==true) {
				$service_provider->operator		= 1;
			} else {
				$service_provider->operator		= 0;
			}

			if (Input::get('channel')==true) {
				$service_provider->channel 		= 1;
			} else {
				$service_provider->channel 		= 0;
			}

			$user->save();
			$service_provider->user_id		= $user->id;
			$service_provider->save();

			

			Session::flash('message', 'Successfully created');
			return Redirect::to('service_provider/index');
		}


	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$provider = ServiceProvider::find($id);
		return View::make('service_provider/edit')
			->with('provider', $provider);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'first_name' 	=> 'required',
			'last_name' 	=> 'required',
			'password'		=> 'between: 4,20',
			'mobile'		=> 'required',
			'join_date'		=> 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('service_provider/' . $id . '/edit')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			$provider = ServiceProvider::find($id);

			$user 		= User::find($provider->user_id);

			// $user->email 			= Input::get('email');
			if (!empty($data['password'])) {
				$user->password 	= Hash::make(Input::get('password'));
			}
			
			$user->role 			= Input::get('role');

			$provider->first_name	= Input::get('first_name');
			$provider->last_name	= Input::get('last_name');
			
			$provider->company		= Input::get('company');
			$provider->join_date	= Input::get('join_date');
			$provider->mobile		= Input::get('mobile');
			// $provider->status 		= ;

			if (Input::get('status')==true) {
				$provider->status 		= 1;
			} else {
				$provider->status 		= 0;
			}

			if (Input::get('employee')==true) {
				$provider->employee		= 1;
			} else {
				$provider->employee		= 0;
			}

			if (Input::get('operator')==true) {
				$provider->operator		= 1;
			} else {
				$provider->operator		= 0;
			}

			if (Input::get('channel')==true) {
				$provider->channel 		= 1;
			} else {
				$provider->channel 		= 0;
			}
			
			

			$user->save();
			$provider->user_id		= $user->id;
			$provider->save();


			Session::flash('message', 'Successfully updated');
			return Redirect::to('service_provider/index');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
