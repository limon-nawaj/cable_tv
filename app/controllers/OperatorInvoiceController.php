<?php

class OperatorInvoiceController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');
		$sub_operators = SubOperator::where('provider_id', $provider->id)->where('status', 1)->get();
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);
		return View::make('operator_invoice.index')->with('area', $area)
					->with('sub_operators', $sub_operators)->with('month', $month)->with('year', $year);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function search_by_area()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');
		
		$area_id = Input::get('area');

		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);
		
		$type = array('monthly'=>'Monthly', 'other'=>'Other');

		// $sub_operator = SubOperator::where('area', $area_id)->where('provider_id', $provider->id)->get();

		
		if ($area_id!='') {
			$sub_operator = SubOperator::where('area', $area_id)->where('provider_id', $provider->id)->get();
		}else {
			$sub_operator = SubOperator::where('provider_id', $provider->id)->get();
		}
		return View::make('operator_invoice.new')->with('area', $area)->with('sub_operators', $sub_operator)
								->with('month', $month)->with('type', $type)->with('year', $year)->with('area_id', $area_id);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$month 	= Input::get('month');
		$year 	= Input::get('year');
		$area_id = Input::get('area_id');
		$type 		= Input::get('type');
		if (!empty($area_id)) {
			$sub_operators = SubOperator::where('area', $area_id)->where('provider_id', $provider->id)->where('status', 1)->get();
		} else {
			$sub_operators = SubOperator::where('provider_id', $provider->id)->where('status', 1)->get();
		}

		$rules 		= array(
				'month' => 'required',
				'year' => 'required',
				'type' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('operator_invoice/create?area='.$area_id)->withErrors($validator);
		} else {
			
			if(isset($_POST['formSubmit'])) 
    		{
				$opt = $_POST['formOperator'];
				
				if(!empty($opt)) 
		        {
					
		            $N = count($opt);

					for($i=0; $i < $N; $i++)
					{
						$sub_operator = SubOperator::find($opt[$i]);
						if ($sub_operator->status == 1) {
					
							// $invoice_id = $sub_operator->id.$month.$year.$type;
							

							$inv = new OperatorInvoice;

							// $inv->invoice_id 	= $invoice_id;
							$inv->provider_id 	= $provider->id;
							$inv->operator_id 	= $sub_operator->id;
							$inv->area_id		= $sub_operator->area;
							$inv->month  		= $month;
							$inv->year 			= $year;
							$inv->vat 			= $sub_operator->monthly_vat;
							$inv->type 			= $type;
							$inv->created_at	= date("Y-m-d");
							// $inv->monthly_fee 	= $sub_operator->monthly_fee;

							$last_bill 			= OperatorInvoice::where('operator_id', $sub_operator->id)->orderBy('id', 'desc')->first();

							$monthly_fee = $sub_operator->monthly_fee + ($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100;
							
							$inv->monthly_fee 	= $monthly_fee;


							$checkInvoice = OperatorInvoice::where('operator_id', $sub_operator->id)->where('month', $month)->where('year', $year)->where('type', $type)->first();
							if (!$checkInvoice) {
								$inv->save();
							}
							
							if (!empty($inv->id)) {
								$bill = new OperatorBill;
								$remine	= OperatorBill::where('operator_id', $sub_operator->id)->orderBy('id', 'desc')->first();
								
								if ($inv->type == 'monthly') {
									$bill->operator_id = $sub_operator->id;
									$bill->invoice_id = $inv->id;
									$bill->created_at = date("Y-m-d");
									if (!empty($remine)) {
										/*if ($remine->debit==0) {
											$bill->debit 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)-$remine->total;
											$bill->total 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)-$remine->total;
										} else {
											$bill->debit 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)+$remine->total;
											$bill->total 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)+$remine->total;
										}*/
										if ($remine->total>=0) {
											$bill->debit 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)+$remine->total;
										} else {
											$bill->debit 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)-$remine->total;
										}
										$bill->total 	= -($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)-$remine->total;
										
									}else{
										$bill->debit 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100);
										$bill->total 	= -($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100);
									}
									$bill->save();
								}
							}
						}
					}
				}
			}
			foreach ($sub_operators as $sub_operator) {
				
			}

			Session::flash('message', 'Successfully created');
			return Redirect::to('operator_bill/index');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
