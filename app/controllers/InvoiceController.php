<?php

class InvoiceController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');
		return View::make('invoice.index')->with('area', $area);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function serce_by_area()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');
		
		$area_id = Input::get('area');

		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		$type = array('monthly'=>'Monthly', 'other'=>'Other');
		
		if ($area_id!='') {
			$customer = Customer::where('area', $area_id)->where('provider_id', $provider->id)->get();
		} else {
			$customer = Customer::where('provider_id', $provider->id)->get();
		}
		

		
		// if (empty($data['area'])) {
		// 	return View::make('invoice.new')->with('area', $area)->with('customers', $customer)
		// 							->with('month', $month)->with('year', $year)
		// 							->with('type', $type)->with('area_id', $area_id);
		// } else {
		// 	$rules = array('area' => 'required');
		// 	$validator = Validator::make(Input::all(), $rules);
		// 	if ($validator->passes()) {
		return View::make('invoice.new')->with('area', $area)->with('customers', $customer)
									->with('month', $month)->with('year', $year)
									->with('type', $type)->with('area_id', $area_id);
			
		// }
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$month 		= Input::get('month');
		$year 		= Input::get('year');
		$area_id 	= Input::get('area_id');
		$type 		= Input::get('type');
		$customers 	= Customer::where('area', $area_id)->where('provider_id', $provider->id)->get();

		$rules 		= array(
				'month' => 'required',
				'year' => 'required',
				'type' => 'required',
				'formCustomer' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('invoice/create?area='.$area_id)->withErrors($validator);
		} else {
			/*foreach ($customers as $customer) {
				if ($customer->status == 1) {
					
					// $invoice_id = $customer->id.$month.$year.$type;
					$checkInvoice = Invoice::where('customer_id', $customer->id)->where('month', $month)->where('year', $year)->where('type', $type)->first();
					if (!$checkInvoice) {
						$inv = new Invoice;

						// $inv->invoice_id 	= $invoice_id;
						$inv->provider_id 	= $provider->id;
						$inv->customer_id 	= $customer->id;
						$inv->area_id		= $customer->area;
						$inv->month  		= $month;
						$inv->year 			= $year;
						$inv->vat 			= $customer->monthly_vat;
						$inv->type 			= $type;
						// $inv->monthly_fee 	= $customer->monthly_fee;

						$last_bill 			= Invoice::where('customer_id', $customer->id)->orderBy('id', 'desc')->first();

						$monthly_fee = $customer->monthly_fee + ($customer->monthly_fee*$customer->monthly_vat)/100;
						
						$inv->monthly_fee 	= $monthly_fee;

						$inv->save();
						$bill = new Bill;
						$remine	= Bill::where('customer_id', $customer->id)->orderBy('id', 'desc')->first();
						
						if ($inv->type == 'monthly') {
							$bill->customer_id = $customer->id;
							$bill->invoice_id = $inv->id;
							if (!empty($remine)) {
								if ($remine->debit==0) {
									$bill->debit 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv-$remine->total;
									$bill->total 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv-$remine->total;
								} else {
									$bill->debit 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv+$remine->total;
									$bill->total 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv+$remine->total;
								}
								
								
							}else{
								$bill->debit 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv;
								$bill->total 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv;
							}
							
							
							$bill->save();
						}
					}
				}
			}*/
			if(isset($_POST['formSubmit'])) 
    		{
				$cust = $_POST['formCustomer'];
				
				if(!empty($cust)) 
		        {
					
		            $N = count($cust);

					for($i=0; $i < $N; $i++)
					{
						// foreach ($customers as $customer) {
						$customer = Customer::find($cust[$i]);
						if ($customer->status == 1) {
							
							// $invoice_id = $customer->id.$month.$year.$type;
							$checkInvoice = Invoice::where('customer_id', $customer->id)->where('month', $month)->where('year', $year)->where('type', $type)->first();
							if (!$checkInvoice) {
								$inv = new Invoice;

								// $inv->invoice_id 	= $invoice_id;
								$inv->provider_id 	= $provider->id;
								$inv->customer_id 	= $customer->id;
								$inv->area_id		= $customer->area;
								$inv->month  		= $month;
								$inv->year 			= $year;
								$inv->vat 			= $customer->monthly_vat;
								$inv->type 			= $type;
								$inv->created_at	= date("Y-m-d");
								// $inv->monthly_fee 	= $customer->monthly_fee;

								$last_bill 			= Invoice::where('customer_id', $customer->id)->orderBy('id', 'desc')->first();

								$monthly_fee = $customer->monthly_fee + ($customer->monthly_fee*$customer->monthly_vat)/100;
								
								$inv->monthly_fee 	= $monthly_fee;

								$inv->save();
								$bill = new Bill;
								$remine	= Bill::where('customer_id', $customer->id)->orderBy('id', 'desc')->first();
								
								if ($inv->type == 'monthly') {
									$bill->customer_id = $customer->id;
									$bill->invoice_id = $inv->id;
									$bill->created_at = date("Y-m-d");
									if (!empty($remine)) {
										/*if ($remine->debit==0) {
											$bill->debit 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv-$remine->total;
											$bill->total 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv-$remine->total;
										} else {
											$bill->debit 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv+$remine->total;
											$bill->total 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv+$remine->total;
										}*/
										if ($remine->total>=0) {
											$bill->debit 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv+$remine->total;
										} else {
											$bill->total 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv-$remine->total;
										}
										$bill->total 	= -($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv-$remine->total;
										
										
									}else{
										$bill->debit 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv;
										$bill->total 	= -($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv;
									}
																	
									$bill->save();
								}
							}
						}
					// }
					}
				}
			} 

			Session::flash('message', 'Successfully created');
			return Redirect::to('bill/index');
		}
		
		
		

		
		/*$rules = array('month'=>'required', 'year'=>'required');

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('invoice/index')->withErrors($validator);
		} else {
			
 		}*/
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
