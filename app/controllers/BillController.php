<?php

class BillController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::user()->role=='service_provider') {
			$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		} elseif(Auth::user()->role=='employee') {
			$employee = Employee::where('user_id', Auth::user()->id)->first();
			$provider = ServiceProvider::find($employee->provider_id);
		}
		
		
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');

		$rules = array('month'=>'required', 'year'=>'required');

		$validator = Validator::make(Input::all(), $rules);

		
		if (Input::get('area')!=null) {
			$invoices = Invoice::orderBy('id', 'desc')->groupBy('customer_id')
			->where('area_id', Input::get('area'))->where('provider_id', $provider->id)->get();
			return View::make('bill.index')->with('month', $month)->with('year', $year)->with('invoices', $invoices)->with('area', $area);	
		}else{
			$invoices = Invoice::orderBy('id', 'desc')->groupBy('customer_id')
			->where('provider_id', $provider->id)->get();	
			return View::make('bill.index')->with('month', $month)->with('year', $year)->with('invoices', $invoices)->with('area', $area);	
		}
	}

	public function search_by_area()
	{
		if (Auth::user()->role=='service_provider') {
			$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		} elseif(Auth::user()->role=='employee') {
			$employee = Employee::where('user_id', Auth::user()->id)->first();
			$provider = ServiceProvider::find($employee->provider_id);
		}
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');

		$rules = array('month'=>'required', 'year'=>'required');

		$validator = Validator::make(Input::all(), $rules);

		$area_id = Input::has('area');
		
		if ($area_id!='') {
			$invoices = Invoice::orderBy('id', 'desc')->groupBy('customer_id')
					->where('area_id', Input::get('area'))->where('provider_id', $provider->id)->get();
		}else{
			$invoices = Invoice::orderBy('id', 'desc')->groupBy('customer_id')->where('provider_id', $provider->id)->get();
		}
		return View::make('bill.search')->with('month', $month)->with('year', $year)
					->with('invoices', $invoices)->with('area', $area)->with('area_id', $area_id);	
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$bill = Invoice::find($id);
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		return View::make('bill.edit')->with('bill', $bill)->with('month', $month)->with('year', $year);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'vat'			=> 'numeric',
			'monthly_fee'	=> 'required|numeric',
			'month'			=> 'required',
			'year'			=> 'required',
			'additional_fee'=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('bill/'.$id.'/edit')->withErrors($validator);
		} else {
			$bill = Invoice::find($id);

			$vat = Input::get('vat');
			$monthly_fee = Input::get('monthly_fee');
			$additional_fee = Input::get('additional_fee');

			$bill->month 			= Input::get('month');
			$bill->year 			= Input::get('year');
			$bill->vat 				= $vat;
			$bill->monthly_fee 		= $monthly_fee;
			$bill->additional_fee 	= $additional_fee;
			$bill->total_fee 		= $monthly_fee + ($monthly_fee*$vat)/100+$additional_fee;

			/*if (Input::get('status')==true) {
				$bill->status 		= 1;
			} else {
				$bill->status 		= 0;
			}*/

			$bill->save();
			Session::flash('message', 'Successfully updated');
			return Redirect::to('bill/index');
		}
		
		
	}

	public function s_edit($id)
	{
		$bill = Invoice::find($id);
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		return View::make('bill.s_edit')->with('bill', $bill)->with('month', $month)->with('year', $year);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function s_update($id)
	{
		$bill = Invoice::find($id);

		$rules = array(
			'vat'			=> 'numeric',
			'monthly_fee'	=> 'required|numeric',
			'month'			=> 'required',
			'year'			=> 'required',
			'additional_fee'=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('bill/search/'.$id.'/edit')->withErrors($validator);
		} else {
			

			$vat = Input::get('vat');
			$monthly_fee = Input::get('monthly_fee');
			$additional_fee = Input::get('additional_fee');

			$bill->month 			= Input::get('month');
			$bill->year 			= Input::get('year');
			$bill->vat 				= $vat;
			$bill->monthly_fee 		= $monthly_fee;
			$bill->additional_fee 	= $additional_fee;
			$bill->total_fee 		= $monthly_fee + ($monthly_fee*$vat)/100+$additional_fee;


			/*if (Input::get('status')==true) {
				$bill->status 		= 1;
			} else {
				$bill->status 		= 0;
			}*/

			$bill->save();
			Session::flash('message', 'Successfully updated');
			return Redirect::to('bill/search?area='.$bill->area_id);
		}
	}

	public function paid()
	{
		$rules = array('credit'=>'required|numeric');
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {
			$bill = new Bill;

			$get_debit = Bill::where('customer_id', Input::get('customer_id'))->where('credit', 0)->orderBy('id', 'desc')->first();
			$get_credit = Bill::where('customer_id', Input::get('customer_id'))->where('debit', 0)->orderBy('id', 'desc')->first();

			$bill->invoice_id = Input::get('invoice_id');
			$bill->customer_id = Input::get('customer_id');
			$bill->credit = Input::get('credit');
			$bill->created_at 		= date("Y-m-d");

			if (!empty($get_credit)) {
				$bill->total = Bill::where('customer_id', Input::get('customer_id'))->where('invoice_id', Input::get('invoice_id'))->where('debit', 0)->orderBy('id', 'desc')->sum('credit')+Input::get('credit')-$get_debit->debit;
			} else {
				$bill->total = Input::get('credit')-$get_debit->debit;
			}
			$bill->save();

			if (Input::get('page')=='individual') {
				return Redirect::to('customer/'.$bill->customer_id);
			} else {
				return Redirect::to('bill/index');
			}
			
		}
		
	}

	public function s_paid()
	{
		$rules = array('credit'=>'required|numeric');
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {
			$bill = new Bill;

			$get_debit = Bill::where('customer_id', Input::get('customer_id'))->where('credit', 0)->orderBy('id', 'desc')->first();
			$get_credit = Bill::where('customer_id', Input::get('customer_id'))->where('debit', 0)->orderBy('id', 'desc')->first();

			$bill->invoice_id = Input::get('invoice_id');
			$bill->customer_id = Input::get('customer_id');
			$bill->credit = Input::get('credit');
			$bill->created_at 		= date("Y-m-d");

			if (!empty($get_credit)) {
				$bill->total = Bill::where('customer_id', Input::get('customer_id'))->where('invoice_id', Input::get('invoice_id'))->where('debit', 0)->orderBy('id', 'desc')->sum('credit')+Input::get('credit')-$get_debit->debit;
			} else {
				$bill->total = Input::get('credit')-$get_debit->debit;
			}
			$bill->save();
			return Redirect::to('bill/search?area='.Input::get('area_id'));

		}
		
	}

	/*public function unpaid()
	{
		$bill = Invoice::find(Input::get('bill_id'));
		$bill->status = 0;
		$bill->save();
		return Redirect::to('bill/index');
	}*/


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
