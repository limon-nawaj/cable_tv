<?php

class AreaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$areas = Area::where('provider_id', $provider->id)->get();
		return View::make('area.index')->with('areas', $areas);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// 
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'name' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('area/index')->withErrors($validator);
		} else {

			$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

			$area 				= new Area;
			$area->name 		= Input::get('name');
			$area->status 		= 1;
			$area->provider_id 	= $provider->id;
			$area->save();
			Session::flash('message', 'Area created successfully');
			return Redirect::to('area/index');
		}
		

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$area = Area::find($id);
		return View::make('area.edit')->with('area', $area);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'name' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('area/'.$id.'/edit')->withErrors($validator);
		} else {
			$area 				= Area::find($id);
			$area->name 		= Input::get('name');
			if (Input::get('status')==true) {
				$area->status 		= 1;
			} else {
				$area->status 		= 0;
			}

			$area->save();
			Session::flash('message', 'Area created successfully');
			return Redirect::to('area/index');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
