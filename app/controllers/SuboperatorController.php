<?php

class SuboperatorController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$sub_operators = SubOperator::where('provider_id', $provider->id)->get();
		return View::make('sub_operator.index')->with('sub_operators', $sub_operators);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->where('status', 1)->lists('name', 'id');
		return View::make('sub_operator.new')->with('area', $area);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$rules = array(
			'opt_id'			=> 'required|between:4, 12|unique:suboperators',
			'first_name' 		=> 'required',
			'last_name' 		=> 'required',
			'flat'				=> 'required',
			'house'				=> 'required',
			'road'				=> 'required',
			'area'				=> 'required',
			'mobile'			=> 'required',
			'connection_date'	=> 'required',
			'connection_fee' 	=> 'numeric',
			'monthly_fee'		=> 'numeric|required',
			'monthly_vat'		=> 'numeric',
			'discount'			=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('sub_operator/create')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			$sub_operator = new SubOperator;

			$sub_operator->opt_id 				= Input::get('opt_id');
			$sub_operator->provider_id 			= $provider->id;
			$sub_operator->first_name 			= Input::get('first_name');
			$sub_operator->last_name 			= Input::get('last_name');
			$sub_operator->flat 				= Input::get('flat');
			$sub_operator->house 				= Input::get('house');
			$sub_operator->road 				= Input::get('road');
			$sub_operator->block 				= Input::get('block');
			$sub_operator->area 				= Input::get('area');
			$sub_operator->phone 				= Input::get('phone');
			$sub_operator->mobile 				= Input::get('mobile');
			$sub_operator->connection_date 		= Input::get('connection_date');
			$sub_operator->connection_fee 		= Input::get('connection_fee');
			$sub_operator->monthly_fee 			= Input::get('monthly_fee');
			$sub_operator->monthly_vat 			= Input::get('monthly_vat');
			$sub_operator->status 				= 1;
			$sub_operator->note 				= Input::get('note');

			$sub_operator->save();

			Session::flash('message', 'Successfully created');
			return Redirect::to('sub_operator/index');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$provider 	= ServiceProvider::where('user_id', Auth::user()->id)->first();
		$sub_operator 	= SubOperator::find($id);
		$operator_invoices 	= OperatorInvoice::where('operator_id', $sub_operator->id)->get();

		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);

		return View::make('sub_operator.show')->with('sub_operator', $sub_operator)
				->with('operator_invoices', $operator_invoices)->with('month', $month)->with('year', $year);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$sub_operator = SubOperator::find($id);
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');
		return View::make('sub_operator.edit')->with('area', $area)->with('sub_operator', $sub_operator);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$rules = array(
			'opt_id'			=> 'required|between:4, 12|unique:suboperators,opt_id,'.$id,
			'first_name' 		=> 'required',
			'last_name' 		=> 'required',
			'house'				=> 'required',
			'road'				=> 'required',
			'area'				=> 'required',
			'mobile'			=> 'required',
			'connection_date'	=> 'required',
			'connection_fee' 	=> 'numeric',
			'monthly_fee'		=> 'numeric|required',
			'monthly_vat'		=> 'numeric',
			'additional_fee'	=> 'numeric',
			'discount'			=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('sub_operator/'.$id.'/edit')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			$sub_operator = SubOperator::find($id);

			$sub_operator->opt_id 				= Input::get('opt_id');
			$sub_operator->provider_id 			= $provider->id;
			$sub_operator->first_name 			= Input::get('first_name');
			$sub_operator->last_name 			= Input::get('last_name');
			$sub_operator->flat 				= Input::get('flat');
			$sub_operator->house 				= Input::get('house');
			$sub_operator->road 				= Input::get('road');
			$sub_operator->block 				= Input::get('block');
			$sub_operator->area 				= Input::get('area');
			$sub_operator->phone 				= Input::get('phone');
			$sub_operator->mobile 				= Input::get('mobile');
			$sub_operator->connection_date 		= Input::get('connection_date');
			$sub_operator->connection_fee 		= Input::get('connection_fee');
			$sub_operator->monthly_fee 			= Input::get('monthly_fee');
			$sub_operator->monthly_vat 			= Input::get('monthly_vat');
			$sub_operator->discount 			= Input::get('discount');
			$sub_operator->status 				= 1;
			$sub_operator->note 				= Input::get('note');

			if (Input::get('status')==true) {
				$sub_operator->status 		= 1;
			} else {
				$sub_operator->status 		= 0;
			}

			$sub_operator->save();

			Session::flash('message', 'Successfully created');
			return Redirect::to('sub_operator/index');
		}
	}


	public function operator_invoice()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$month 		= Input::get('month');
		$year 		= Input::get('year');
		$type 		= Input::get('type');
		$operator_id = Input::get('operator_id');

		$rules 		= array(
			'month' => 'required',
			'year' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('sub_operator/'.$operator_id)->withErrors($validator);
		} else{
			$sub_operator = SubOperator::find($operator_id);
			if ($sub_operator->status == 1) {
		
				// $invoice_id = $sub_operator->id.$month.$year.$type;
				

				$inv = new OperatorInvoice;

				// $inv->invoice_id 	= $invoice_id;
				$inv->provider_id 	= $provider->id;
				$inv->operator_id 	= $sub_operator->id;
				$inv->area_id		= $sub_operator->area;
				$inv->month  		= $month;
				$inv->year 			= $year;
				$inv->vat 			= $sub_operator->monthly_vat;
				$inv->type 			= $type;
				// $inv->monthly_fee 	= $sub_operator->monthly_fee;

				$last_bill 			= OperatorInvoice::where('operator_id', $sub_operator->id)->orderBy('id', 'desc')->first();

				$monthly_fee = $sub_operator->monthly_fee + ($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100;
				
				$inv->monthly_fee 	= $monthly_fee;


				$checkInvoice = OperatorInvoice::where('operator_id', $sub_operator->id)->where('month', $month)->where('year', $year)->where('type', $type)->first();
				if (!$checkInvoice) {
					$inv->save();
				}
				
				if (!empty($inv->id)) {
					$bill = new OperatorBill;
					$remine	= OperatorBill::where('operator_id', $sub_operator->id)->orderBy('id', 'desc')->first();
					
					if ($inv->type == 'monthly') {
						$bill->operator_id = $sub_operator->id;
						$bill->invoice_id = $inv->id;
						if (!empty($remine)) {
							if ($remine->debit==0) {
								$bill->debit 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)-$remine->total;
								$bill->total 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)-$remine->total;
							} else {
								$bill->debit 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)+$remine->total;
								$bill->total 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100)+$remine->total;
							}
						}else{
							$bill->debit 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100);
							$bill->total 	= ($sub_operator->monthly_fee+($sub_operator->monthly_fee*$sub_operator->monthly_vat)/100);
						}
						$bill->save();
					}
				}
			}
			return Redirect::to('sub_operator/'.$operator_id);
		}
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
