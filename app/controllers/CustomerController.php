<?php

class CustomerController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$customers = Customer::where('provider_id', $provider->id)->get();
		return View::make('customer.index')->with('customers', $customers);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->where('status', 1)->lists('name', 'id');
		return View::make('customer.new')->with('area', $area);
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$rules = array(
			'cust_id'			=> 'required|between:4, 12|unique:customers',
			'first_name' 		=> 'required',
			'last_name' 		=> 'required',
			'flat'				=> 'required',
			'house'				=> 'required',
			'road'				=> 'required',
			'area'				=> 'required',
			'mobile'			=> 'required',
			'connection_date'	=> 'required',
			'connection_fee' 	=> 'numeric',
			'num_of_tv'			=> 'integer|required',
			'monthly_fee'		=> 'numeric|required',
			'monthly_vat'		=> 'numeric',
			'additional_fee'	=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('customer/create')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			$customer = new Customer;
			$customer->cust_id 				= Input::get('cust_id');
			$customer->provider_id 			= $provider->id;
			$customer->first_name 			= Input::get('first_name');
			$customer->last_name 			= Input::get('last_name');
			$customer->flat 				= Input::get('flat');
			$customer->house 				= Input::get('house');
			$customer->road 				= Input::get('road');
			$customer->block 				= Input::get('block');
			$customer->area 				= Input::get('area');
			$customer->phone 				= Input::get('phone');
			$customer->mobile 				= Input::get('mobile');
			$customer->connection_date 		= Input::get('connection_date');
			$customer->connection_fee 		= Input::get('connection_fee');
			$customer->num_of_tv 			= Input::get('num_of_tv');
			$customer->monthly_fee 			= Input::get('monthly_fee');
			$customer->monthly_vat 			= Input::get('monthly_vat');
			$customer->additional_fee 		= Input::get('additional_fee');
			$customer->status 				= 1;
			$customer->note 				= Input::get('note');

			$customer->save();

			Session::flash('message', 'Successfully created');
			return Redirect::to('customer/index');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$provider 	= ServiceProvider::where('user_id', Auth::user()->id)->first();
		$customer 	= Customer::find($id);
		$invoices 	= Invoice::where('customer_id', $customer->id)->get();

		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);
		return View::make('customer.show')->with('customer', $customer)
				->with('invoices', $invoices)->with('month', $month)->with('year', $year);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$customer = Customer::find($id);
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');
		return View::make('customer.edit')->with('area', $area)->with('customer', $customer);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$rules = array(
			'cust_id'			=> 'required|between:4, 12|unique:customers,cust_id,'.$id,
			'first_name' 		=> 'required',
			'last_name' 		=> 'required',
			'house'				=> 'required',
			'road'				=> 'required',
			'area'				=> 'required',
			'mobile'			=> 'required',
			'connection_date'	=> 'required',
			'connection_fee' 	=> 'numeric',
			'num_of_tv'			=> 'integer|required',
			'monthly_fee'		=> 'numeric|required',
			'monthly_vat'		=> 'numeric',
			'additional_fee'	=> 'numeric'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('customer/'.$id.'/edit')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			$customer = Customer::find($id);
			$customer->cust_id 				= Input::get('cust_id');
			$customer->provider_id 			= $provider->id;
			$customer->first_name 			= Input::get('first_name');
			$customer->last_name 			= Input::get('last_name');
			$customer->flat 				= Input::get('flat');
			$customer->house 				= Input::get('house');
			$customer->road 				= Input::get('road');
			$customer->block 				= Input::get('block');
			$customer->area 				= Input::get('area');
			$customer->phone 				= Input::get('phone');
			$customer->mobile 				= Input::get('mobile');
			$customer->connection_date 		= Input::get('connection_date');
			$customer->connection_fee 		= Input::get('connection_fee');
			$customer->num_of_tv 			= Input::get('num_of_tv');
			$customer->monthly_fee 			= Input::get('monthly_fee');
			$customer->monthly_vat 			= Input::get('monthly_vat');
			$customer->additional_fee 		= Input::get('additional_fee');
			$customer->note 				= Input::get('note');

			if (Input::get('status')==true) {
				$customer->status 		= 1;
			} else {
				$customer->status 		= 0;
			}

			$customer->save();

			Session::flash('message', 'Successfully created');
			return Redirect::to('customer/index');
		}
	}

	public function generate_invoice()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$month 		= Input::get('month');
		$year 		= Input::get('year');
		$type 		= Input::get('type');
		$customer_id = Input::get('customer_id');

		$rules 		= array(
			'month' => 'required',
			'year' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('customer/'.$customer_id)->withErrors($validator);
		} else {
			$customer = Customer::find($customer_id);
			if ($customer->status == 1) {
				
				$checkInvoice = Invoice::where('customer_id', $customer->id)->where('month', $month)->where('year', $year)->where('type', $type)->first();
				if (!$checkInvoice) {
					$inv = new Invoice;

					// $inv->invoice_id 	= $invoice_id;
					$inv->provider_id 	= $provider->id;
					$inv->customer_id 	= $customer->id;
					$inv->area_id		= $customer->area;
					$inv->month  		= $month;
					$inv->year 			= $year;
					$inv->vat 			= $customer->monthly_vat;
					$inv->type 			= $type;
					// $inv->monthly_fee 	= $customer->monthly_fee;

					$last_bill 			= Invoice::where('customer_id', $customer->id)->orderBy('id', 'desc')->first();

					$monthly_fee = $customer->monthly_fee + ($customer->monthly_fee*$customer->monthly_vat)/100;
					
					$inv->monthly_fee 	= $monthly_fee;

					$inv->save();
					$bill = new Bill;
					$remine	= Bill::where('customer_id', $customer->id)->orderBy('id', 'desc')->first();
					
					if ($inv->type == 'monthly') {
						$bill->customer_id = $customer->id;
						$bill->invoice_id = $inv->id;
						if (!empty($remine)) {
							if ($remine->debit==0) {
								$bill->debit 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv-$remine->total;
								$bill->total 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv-$remine->total;
							} else {
								$bill->debit 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv+$remine->total;
								$bill->total 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv+$remine->total;
							}
							
						}else{
							$bill->debit 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv;
							$bill->total 	= ($customer->monthly_fee+($customer->monthly_fee*$customer->monthly_vat)/100)*$customer->num_of_tv;
						}
						$bill->save();
					}
				}
			}
			return Redirect::to('customer/'.$customer_id);
		}
		// return Redirect::to('customer/'.$customer_id);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
