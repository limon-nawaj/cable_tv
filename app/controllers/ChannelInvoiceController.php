<?php

class ChannelInvoiceController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');
		$pay_channels = PayChannel::where('provider_id', $provider->id)->where('status', 1)->get();
		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);
		return View::make('channel_invoice.index')->with('area', $area)
					->with('pay_channels', $pay_channels)->with('month', $month)->with('year', $year);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();
		$area = array('' => 'Select Area') + Area::select(DB::raw('concat (name) as name, id'))->where('provider_id', $provider->id)->lists('name', 'id');
		
		$area_id = Input::get('area');

		$month = array(''=>'Select Month', 
			'January'=>'January', 'February'=>'February', 'March'=>'March','April'=>'April','May'=>'May','June'=>'June',
			'July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
		$year = array(''=>'Select Year', 2014=>2014, 2015=>2015, 2016=>2016, 2017=>2017, 
			2018=>2018, 2019=>2019, 2020=>2020, 2021=>2021, 2022=>2022, 2023=>2023, 2024=>2024, 
			2025=>2025, 2026=>2026, 2027=>2027, 2028=>2028, 2029=>2029, 2030=>2030);
		
		$pay_channel = PayChannel::where('area', $area_id)->where('provider_id', $provider->id)->get();
		
		if (empty($data['area'])) {
			return View::make('channel_invoice.new')->with('area', $area)->with('pay_channels', $pay_channel)
									->with('month', $month)->with('year', $year)->with('area_id', $area_id);
		} else {
			$rules = array('area' => 'required');
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->passes()) {
				return View::make('channel_invoice.new')->with('area', $area)->with('pay_channels', $pay_channel)
									->with('month', $month)->with('year', $year)->with('area_id', $area_id);
			}
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$provider = ServiceProvider::where('user_id', Auth::user()->id)->first();

		$month 		= Input::get('month');
		$year 		= Input::get('year');
		$type 		= Input::get('type');
		$channels 	= PayChannel::where('provider_id', $provider->id)->get();

		$rules 		= array(
				'month' => 'required',
				'year' => 'required',
				'formChannel' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('channel_invoice/index')->withErrors($validator);
		} else {
			
			if(isset($_POST['formSubmit'])) 
    		{
				$chan = $_POST['formChannel'];
				
				if(!empty($chan)) 
		        {
					
		            $N = count($chan);

					for($i=0; $i < $N; $i++)
					{
						$channel = PayChannel::find($chan[$i]);
						if ($channel->status == 1) {
							
							// $invoice_id = $channel->id.$month.$year.$type;
							$checkInvoice = ChannelInvoice::where('channel_id', $channel->id)->where('month', $month)->where('year', $year)->where('type', $type)->first();
							if (!$checkInvoice) {
								$inv = new ChannelInvoice;

								// $inv->invoice_id 	= $invoice_id;
								$inv->provider_id 	= $provider->id;
								$inv->channel_id 	= $channel->id;
								$inv->month  		= $month;
								$inv->year 			= $year;
								$inv->vat 			= $channel->monthly_vat;
								$inv->type 			= $type;
								$inv->created_at	= date("Y-m-d");
								// $inv->monthly_fee 	= $channel->monthly_fee;

								$last_bill 			= ChannelInvoice::where('channel_id', $channel->id)->orderBy('id', 'desc')->first();

								$monthly_fee = $channel->monthly_fee + ($channel->monthly_fee*$channel->monthly_vat)/100;
								
								$inv->monthly_fee 	= $monthly_fee;

								$inv->save();
								$bill = new ChannelBill;
								$remine	= ChannelBill::where('channel_id', $channel->id)->orderBy('id', 'desc')->first();
								
								if ($inv->type == 'monthly') {
									$bill->channel_id = $channel->id;
									$bill->invoice_id = $inv->id;
									$bill->created_at = date("Y-m-d");
									if (!empty($remine)) {
										if ($remine->total>=0) {
											$bill->debit 	= ($channel->monthly_fee+($channel->monthly_fee*$channel->monthly_vat)/100)+$remine->total;
										}else{
											$bill->debit 	= ($channel->monthly_fee+($channel->monthly_fee*$channel->monthly_vat)/100)-$remine->total;
										}
										$bill->total 	= -($channel->monthly_fee+($channel->monthly_fee*$channel->monthly_vat)/100)+$remine->total;
										
									}else{
										$bill->debit 	= ($channel->monthly_fee+($channel->monthly_fee*$channel->monthly_vat)/100);
										$bill->total 	= -($channel->monthly_fee+($channel->monthly_fee*$channel->monthly_vat)/100);
									}
									
									
									$bill->save();
								}
							}
						}
					// }
					}
				}
			} 
			
			Session::flash('message', 'Successfully created');
			return Redirect::to('channel_bill/index');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
