<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function($table){
			$table->increments('id');
			$table->string('cust_id');
			$table->integer('provider_id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('flat');
			$table->string('house');
			$table->string('road');
			$table->string('block');
			$table->integer('area');
			$table->string('phone');
			$table->string('mobile');
			$table->date('connection_date');
			$table->double('connection_fee');
			$table->integer('num_of_tv');
			$table->double('monthly_fee');
			$table->double('monthly_vat');
			$table->double('additional_fee');
			$table->double('discount');
			$table->string('status');
			$table->string('note');
			// $table->string('type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
