<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelBillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('channel_bills', function($table){
			$table->increments('id');
			$table->integer('channel_id');
			$table->integer('invoice_id');
			$table->string('type');
			$table->date('payment_date');
			$table->double('debit');
			$table->double('credit');
			$table->double('total');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('channel_bills');
	}

}
