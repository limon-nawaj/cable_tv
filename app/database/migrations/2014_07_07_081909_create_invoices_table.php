<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function($table){
			$table->increments('id');
			$table->integer('provider_id');
			$table->integer('customer_id');
			$table->integer('area_id');
			$table->string('month');
			$table->integer('year');
			$table->double('vat');
			$table->double('monthly_fee');
			$table->double('additional_fee');
			$table->double('total_fee');
			$table->string('type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}
