<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('channel_invoices', function($table){
			$table->increments('id');
			$table->integer('provider_id');
			$table->integer('channel_id');
			$table->integer('area_id');
			$table->string('month');
			$table->integer('year');
			$table->double('vat');
			$table->double('monthly_fee');
			$table->double('additional_fee');
			$table->double('total_fee');
			$table->string('type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('channel_invoices');
	}

}
