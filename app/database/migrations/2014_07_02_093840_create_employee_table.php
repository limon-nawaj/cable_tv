<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function($table){
			$table->increments('id');
			$table->integer('provider_id');
			$table->integer('user_id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('present_address');
			$table->string('permanent_address');
			$table->string('mobile');
			$table->integer('designation_id');
			$table->date('join_date');
			$table->double('basic_salary');
			$table->double('other_salary');
			$table->integer('status');
			$table->string('note');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
