<?php

class Employee extends BaseModel {

    protected $table = 'employees';

    public $timestamps = false;

	public function provider()
	{
		return $this->belongsTo('ServiceProvider');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function designation()
	{
		return $this->belongsTo('Designation');
	}

}