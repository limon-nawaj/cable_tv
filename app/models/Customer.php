<?php

class Customer extends BaseModel {

    protected $table = 'customers';

    public $timestamps = false;

	public function provider()
	{
		return $this->belongsTo('ServiceProvider');
	}

	public function area()
	{
		return $this->belongsTo('Area');
	}

	public function invoice()
	{
		return $this->hasMany('Invoice');
	}

}