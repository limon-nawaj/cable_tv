<?php

class Admin extends BaseModel {

    protected $table = 'admins';

    public $timestamps = false;

    public static $rules = array(
		'first_name' => 'required',
		'last_name' => 'required',
		'email' => 'required|unique:presenters'
	);

	public function user()
	{
		return $this->belongsTo('User');
	}

}