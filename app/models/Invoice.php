<?php

class Invoice extends BaseModel {

    protected $table = 'invoices';

    public $timestamps = false;

	public function provider()
	{
		return $this->belongsTo('ServiceProvider');
	}

	public function customer()
	{
		return $this->belongsTo('Customer');
	}

}