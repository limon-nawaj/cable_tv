<?php

class Area extends BaseModel {

    protected $table = 'areas';

    public $timestamps = false;

	public function provider()
	{
		return $this->belongsTo('ServiceProvider');
	}

	public function customer()
	{
		return $this->hasMany('Customer');
	}

}