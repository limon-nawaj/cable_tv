<?php

class Designation extends BaseModel {

    protected $table = 'designations';

    public $timestamps = false;

	public function provider()
	{
		return $this->belongsTo('ServiceProvider');
	}

	public function employee()
	{
		return $this->hasOne('Employee');
	}
}