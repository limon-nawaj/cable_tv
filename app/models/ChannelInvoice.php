<?php

class ChannelInvoice extends BaseModel {

    protected $table = 'channel_invoices';

    public $timestamps = false;

	public function provider()
	{
		return $this->belongsTo('ServiceProvider');
	}

	public function customer()
	{
		return $this->belongsTo('Customer');
	}

}