<?php

class OperatorInvoice extends BaseModel {

    protected $table = 'operator_invoices';

    public $timestamps = false;

	public function provider()
	{
		return $this->belongsTo('ServiceProvider');
	}

	public function customer()
	{
		return $this->belongsTo('Customer');
	}

}