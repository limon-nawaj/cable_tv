<?php

class Bill extends BaseModel {

    protected $table = 'bills';

    public $timestamps = false;

	
	public function customer()
	{
		return $this->hasMany('Customer');
	}

}