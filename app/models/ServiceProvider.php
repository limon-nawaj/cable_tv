<?php

class ServiceProvider extends BaseModel {

    protected $table = 'serivce_providers';

    public $timestamps = false;

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function area()
	{
		return $this->hasMany('Area');
	}

	public function designation()
	{
		return $this->hasMany('Designation');
	}
}